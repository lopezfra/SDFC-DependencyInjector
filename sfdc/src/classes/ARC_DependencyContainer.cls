/**
 * The global dependency container
 */
public interface ARC_DependencyContainer
{
    ARC_Service_Account getAccountService ();

    ARC_Service_Assortment getAssortmentService();

    ARC_Service_CatalogueItem getCatalogueItemService();

    ARC_Service_Category getCategoryService ();

    ARC_Service_CategoryMedia getCategoryMediaService ();

    ARC_Service_Colors getColorService ();

    ARC_Configuration getConfiguration ();

    ARC_Service_Error getErrorService ();

    ARC_Service_DeliveryBlock getDeliveryBlockService ();

    ARC_Service_OrderBlock getOrderBlockService ();

    ARC_Service_i18nL18n geti18nL18nService ();

    ARC_Service_Listing getListingService ();

    ARC_Service_Message getMessageService();

    ARC_Service_Package getPackageService();

    ARC_Service_Pricing getPricingService ();

    ARC_Service_ProductRecommendationEngine getProductRecommendationEngine();

    ARC_Service_ProductCategory getProductCategoryService();

    ARC_Service_ProductList getProductListService ();

    ARC_Service_Product getProductService ();

    ARC_Service_ProductDetails getProductDetailsService ();

    ARC_Service_ProductFilter getProductFilterService ();

    ARC_Service_ProductStatus getProductStatusService ();

    ARC_Service_SalesOrg getSalesOrgService ();

    ARC_Service_Sizes getSizeService ();

    ARC_Service_Season getSeasonService();

    ARC_Service_Security getSecurityService ();

    ARC_Service_Spec getSpecService ();

    ARC_Service_Storefront getStorefrontService ();

    ARC_Service_ProductAvailability getProductAvailabilityService();

    ARC_Service_Order getOrderService();

    ARC_Service_OrderSubmission getOrderSubmissionService();

    ARC_Service_Cart getCartService();

    ARC_Service_Contact getContactService ();

    ARC_Service_User getUserService();

    ARC_Service_ContactAddress getContactAddresssService ();

    ARC_API_Order getOrderAPI();

    ARC_Service_ContentPage getContentPageService();

    ARC_Cache getCache();

    ARC_Cache getGlobalCacheService();

    ARC_Service_Resource getResourceService();


    // ------------ data table gateways
    ARC_TDGW_Account getAccountDataGateway ();

    ARC_TDGW_Cart getCartDataGateway ();

    ARC_TDGW_Category getCategoryDataGateway ();

    ARC_TDGW_Contact getContactDataGateway ();

    ARC_TDGW_Inventory getInventoryDataGateway ();

    ARC_TDGW_PriceList getPriceListDataGateway ();

    ARC_TDGW_PriceListItem getPricelistItemDataGateway ();

    ARC_TDGW_Product getProductDataGateway ();

    ARC_TDGW_ProductStatus getProductStatusDataGateway ();

    ARC_TDGW_Season getSeasonDataGateway ();

    ARC_TDGW_User getUserDataGateway ();

    ARC_TDGW_ProductStorefront getProductStorefrontDataGateway ();

}