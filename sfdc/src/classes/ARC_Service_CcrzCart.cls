/**
 * Provides business logic around the cart entity and wraps CCRZ business logic
 */
public with sharing class ARC_Service_CcrzCart implements ARC_Service_Cart
{
    private static final Integer EAN_CODE_LENGTH = 13;

    private static final Integer UPC_CODE_LENGTH = 12;

    private static final String defaultCartName = 'Personal reference';

    private ARC_Configuration_Cart cartConfiguration;

    private ARC_TDGW_Cart cartDataGateway;

    private ARC_Service_SalesOrg salesOrgService;

    private ARC_Service_Order orderService;

    private ARC_Service_Error errorService;

    private ARC_Service_Product productService;

    private ARC_Service_Account accountService;

    private ARC_Service_ProductDetails productDetailService;

    private ARC_Service_i18nL18n i18nL18nService;

    private ARC_Service_User userService;


    /**
     * @deprected DO NOT USE empty constructors - obey DI and its container
     */
    public ARC_Service_CcrzCart ()
    {
        this(
            ARC_Dependencies.container.getConfiguration().getCartConfiguration(),
            ARC_Dependencies.container.getCartDataGateway(),
            ARC_Dependencies.container.getSalesOrgService(),
            ARC_Dependencies.container.getAccountService(),
            ARC_Dependencies.container.getOrderService(),
            ARC_Dependencies.container.getErrorService(),
            ARC_Dependencies.container.getProductService(),
            ARC_Dependencies.container.getProductDetailsService(),
            ARC_Dependencies.container.geti18nL18nService(),
            ARC_Dependencies.container.getUserService()
        );
    }


    public ARC_Service_CcrzCart (
        ARC_Configuration_Cart cartConfiguration,
        ARC_TDGW_Cart cartDataGateway,
        ARC_Service_SalesOrg salesOrgService,
        ARC_Service_Account accountService,
        ARC_Service_Order orderService,
        ARC_Service_Error errorService,
        ARC_Service_Product productService,
        // TODO This one needs to go
        ARC_Service_ProductDetails productDetailService,
        ARC_Service_i18nL18n i18nL18nService,
        ARC_Service_User userService
    )
    {
        this.cartConfiguration = cartConfiguration;
        this.cartDataGateway = cartDataGateway;
        this.salesOrgService = salesOrgService;
        this.accountService = accountService;
        this.orderService = orderService;
        this.errorService = errorService;
        this.productService = productService;
        this.productDetailService = productDetailService;
        this.i18nL18nService = i18nL18nService;
        this.userService = userService;
    }

    /**
     * @return Initialized cart
     */
    public ccrz__E_Cart__c initializeCart (String encryptedId)
    {
        // create new cart
        if (encryptedId == null) {

            Map<String,Object> createResults = ccrz.ccApiCart.create(
                new Map<String, Object>{
                    ccrz.ccApi.API_VERSION => 4,
                    ccrz.ccApiCart.CART_OBJLIST => new List<Map<String, Object>> { new Map<String,Object>() }
                }
            );
            encryptedId = (String) createResults.get(ccrz.ccApiCart.CART_ENCID);
        }

        ccrz__E_Cart__c cart = [
            SELECT
                ccrz__EncryptedId__c,
                ccrz__Storefront__c,
                SalesOrg__c,
                ShipTo__c
            FROM
                ccrz__E_Cart__c
            WHERE
                ccrz__EncryptedId__c = :encryptedId
        ];

        // Values to be set only if not set
        Boolean isCartChanged = false;
        if (cart.SalesOrg__c == null) {
            cart.SalesOrg__c = this.salesOrgService.getCurrentSalesOrg(cart.ccrz__Storefront__c).Id;
            isCartChanged = true;
        }
        if (cart.ShipTo__c == null) {
            cart.ShipTo__c = this.accountService.getBrandSpecificDefaultShipTo(this.accountService.getCurrentAccount(),cart.ccrz__Storefront__c);
            isCartChanged = true;
        }

        if (isCartChanged) {
            update cart;
        }

        return cart;
    }

    /**
     * Will execute the net price calculation utilizing the order service functionality
     */
    public ccrz__E_Cart__c calculateNetPrices (ccrz__E_Cart__c cart)
    {
        List<ccrz__E_CartItem__c> cartItems = getCartItemsForOrder(cart);
        ARC_DTO_Order.Order orderDTO = orderService.calculatePrice( createServiceOrder(cart, cartItems) );

        System.SavePoint savePoint = Database.setSavepoint();
        try {
            updateCart(cart, cartItems, orderDTO);
        } catch(Exception e) {
            Database.rollback(savePoint);
            throw e;
        }
        return cart;
    }

    private ARC_DTO_Order.Order createServiceOrder (ccrz__E_Cart__c cart, List<ccrz__E_CartItem__c> cartItems)
    {
        return createServiceOrder(cart, cartItems, cart.Id);
    }

    public ARC_DTO_Order.Order createServiceOrder (ccrz__E_Cart__c cart, List<ccrz__E_CartItem__c> cartItems, Id orderId)
    {
        ARC_DTO_Order.Order order = new ARC_DTO_Order.Order();
        order.internalId = orderId;
        order.externalUniqueId = getExternalId(orderId);
        order.personalReference = cart.ccrz__Name__c;
        order.storefront = cart.ccrz__Storefront__c;
        order.salesOrg = cart.SalesOrg__r.EUID__c;
        order.soldTo = cart.ccrz__Account__r.AccountNumber;
        order.shipTo = cart.ShipTo__r.AccountNumber;
        order.orderDateTime = DateTime.now();
        order.deliveryDate = Date.today();

        createServiceOrderItems(order, cartItems, order.salesOrg);

        return order;
    }

    private String makeProductDetailEuid(String sku, String technicalSize, String salesOrg) {
        return sku + (technicalSize != null ? '-' + technicalSize : '') + '-' + salesOrg;
    }

    private void createServiceOrderItems (ARC_DTO_Order.Order order, List<ccrz__E_CartItem__c> cartItems, String salesOrg)
    {
        Date earliestDeliveryDate = null;
        Map<String, ARC_DTO_Order.OrderItem> articleNumberToOrderItemMap = new Map<String, ARC_DTO_Order.OrderItem>();

        Set<String> productDetailEuidSet = new Set<String>();
        for (ccrz__E_CartItem__c cartItem : cartItems) {
            String parentArticleNumber = cartItem.ccrz__Product__r.ccrz__ParentProduct__r.ArticleNumber__c;
            String technicalSize = getTechnicalSize(cartItem);
            String productDetailEuid = makeProductDetailEuid(parentArticleNumber, technicalSize, salesOrg);

            productDetailEuidSet.add(productDetailEuid);
        }
        Map<String, LE_SalesOrgProductDetails__c> euidProductDetailMap =
                productDetailService.getProductDetailsByEuid(productDetailEuidSet);

        for (ccrz__E_CartItem__c cartItem : cartItems) {
            String parentArticleNumber = cartItem.ccrz__Product__r.ccrz__ParentProduct__r.ArticleNumber__c;
            String technicalSize = getTechnicalSize(cartItem);
            String productDetailEuid = makeProductDetailEuid(parentArticleNumber, technicalSize, salesOrg);

            ARC_DTO_Order.OrderItem orderItem = articleNumberToOrderItemMap.get(parentArticleNumber);
            if (orderItem == null) {
                orderItem = new ARC_DTO_Order.OrderItem();
                orderItem.itemNumber = order.items.size() + 1;
                orderItem.articleNumber = parentArticleNumber;
                if (euidProductDetailMap != null) {
                    LE_SalesOrgProductDetails__c productDetails = euidProductDetailMap.get(productDetailEuid);
                    orderItem.deliveryPlant = (productDetails != null ? productDetails.DefaultDeliveryPlant__c : null);
                }
                order.items.add(orderItem);
                articleNumberToOrderItemMap.put(parentArticleNumber, orderItem);
            }

            ARC_DTO_Order.OrderLine orderLine = new ARC_DTO_Order.OrderLine();
            orderLine.lineNumber = orderItem.lines.size() + 1;
            if (cartItem.ccrz__Product__r.EAN__c != null) {
                orderLine.eanOrUpc = cartItem.ccrz__Product__r.EAN__c;
            } else if (cartItem.ccrz__Product__r.UPC__c != null) {
                orderLine.eanOrUpc = cartItem.ccrz__Product__r.UPC__c;
            } else { // case should not happen (just to not loose any information)
                orderLine.eanOrUpc = cartItem.ccrz__Product__r.ArticleNumber__c;
            }
            orderLine.technicalSize = technicalSize;
            orderLine.quantity = Integer.valueOf(cartItem.ccrz__Quantity__c);
            orderLine.lineNetTotal = cartItem.ccrz__ItemTotal__c;
            orderLine.deliveryDate = (cartItem.ccrz__RequestDate__c != null ? cartItem.ccrz__RequestDate__c : Date.today());
            orderLine.internalId = ARC_DTO_Order.generateOrderLineInternalId(order, orderItem, orderLine);
            orderItem.lines.add(orderLine);

            cartItem.ccrz__CartItemId__c = orderLine.internalId;

            if (earliestDeliveryDate == null || orderLine.deliveryDate < earliestDeliveryDate) {
                earliestDeliveryDate = orderLine.deliveryDate;
            }
        }

        if (earliestDeliveryDate != null) {
            order.deliveryDate = earliestDeliveryDate;
        }
    }

    private String getTechnicalSize(ccrz__E_CartItem__c cartItem) {
        final String technicalSize = null;
        if (cartItem.ccrz__Product__r != null
            && cartItem.ccrz__Product__r.ccrz__Sku__c != null
            && cartItem.ccrz__Product__r.ccrz__Sku__c.contains('-'))
        {
            technicalSize = cartItem.ccrz__Product__r.ccrz__Sku__c.split('-')[1];
        }
        return technicalSize;
    }

    public List<ccrz__E_CartItem__c> getCartItemsForOrder (ccrz__E_Cart__c cart)
    {
        return [
            SELECT
                Id,
                ccrz__Quantity__c,
                ccrz__Product__r.EAN__c,
                ccrz__Product__r.UPC__c,
                ccrz__CartItemId__c,
                ccrz__Product__r.ArticleNumber__c,
                ccrz__Product__r.ccrz__SKU__c,
                ccrz__Product__r.ccrz__ParentProduct__r.ccrz__SKU__c,
                ccrz__Product__r.ccrz__ParentProduct__r.ArticleNumber__c,
                ccrz__RequestDate__c,
                ccrz__SubAmount__c,
                ccrz__ItemTotal__c
            FROM
                ccrz__E_CartItem__c
            WHERE
	            ccrz__Cart__c = :cart.Id
	            AND ccrz__Product__r.ccrz__ProductType__c = :ARC_Constants.PRODUCT_TYPE_EAN
        ];
    }

    private String getExternalId (Id cartId)
    {
    	String externalId = null;

    	List<ccrz__E_Order__c> orderList =[
            SELECT
                ccrz__OrderId__c,
                ccrz__OriginatedCart__c
            FROM
                ccrz__E_Order__c
            WHERE
                ccrz__OriginatedCart__c =: cartId
        ];

    	if (orderList != null && !orderList.isEmpty()) {
    		externalId = orderList[0].ccrz__OrderId__c;
    	}

    	return externalId;
    }

    public void updateCart(ccrz__E_Cart__c cart, List<ccrz__E_CartItem__c> cartItems, ARC_DTO_Order.Order order) {
        updateCartItems(cart.Id, cartItems, order);

        cart.ccrz__TaxAmount__c = computeVatTotal(order);
        update cart;
    }

    private Decimal computeVatTotal(ARC_DTO_Order.Order order) {
        Decimal vatTotal = 0.0;
        for (ARC_DTO_Order.OrderItem orderItem : order.items) {
            if (orderItem.itemVatTotal != null) {
                vatTotal += orderItem.itemVatTotal;
            }
        }
        return vatTotal;
    }

    private void deleteAllPackagesFromCart(Id cartId) {


        List<ccrz__E_CartItem__c> packageCartItems = [
                SELECT
                        Id,
                        GroupId__c,
                        ccrz__Product__r.ccrz__ProductType__c
                FROM
                        ccrz__E_CartItem__c
                WHERE
                ccrz__Cart__c = :cartId
                AND GroupId__c != null
        ];

        if(packageCartItems == null || packageCartItems.isEmpty()) {
            return;
        }

        List<ccrz__E_CartItem__c> packageCartItemsPackage = new List<ccrz__E_CartItem__c>();
        List<ccrz__E_CartItem__c> packageCartItemsMajor = new List<ccrz__E_CartItem__c>();

        for(ccrz__E_CartItem__c packageCartItem : packageCartItems ){

            if(packageCartItem.ccrz__Product__r.ccrz__ProductType__c == ARC_Constants.PRODUCT_TYPE_PACKAGE){
                packageCartItemsPackage.add(packageCartItem);
            }
            else {
                packageCartItem.GroupId__c = null;
                packageCartItemsMajor.add(packageCartItem);
            }

        }

        if(!packageCartItemsMajor.isEmpty()) {
            delete packageCartItemsPackage;
        }
        if(!packageCartItemsMajor.isEmpty()) {
            update packageCartItemsMajor;
        }

    }

    /**
     * Merges a cart item into a list of cart items.
     * Adds the item to the list, except in case of an existing duplicate.
     * A duplicate cart item is merged into an existing one.
     * Returns the given cart item if added, and the merged cart item if merged.
     **/
    private ccrz__E_CartItem__c mergeCartItemIntoList(List<ccrz__E_CartItem__c> cartItems, ccrz__E_CartItem__c cartItem) {
        ccrz__E_CartItem__c cartItemToMergeWith = null;
        for (ccrz__E_CartItem__c existingCartItem : cartItems) {
            if (existingCartItem.ccrz__Product__c == cartItem.ccrz__Product__c
                && existingCartItem.ccrz__RequestDate__c == cartItem.ccrz__RequestDate__c)
            {
                cartItemToMergeWith = existingCartItem;
                break;
            }
        }
        if (cartItemToMergeWith == null) {
            cartItems.add(cartItem);
            return cartItem;
        } else {
            cartItemToMergeWith.ccrz__SubAmount__c += cartItem.ccrz__SubAmount__c;
            cartItemToMergeWith.ccrz__Quantity__c += cartItem.ccrz__Quantity__c;
            cartItemToMergeWith.ccrz__Price__c = cartItemToMergeWith.ccrz__SubAmount__c / cartItemToMergeWith.ccrz__Quantity__c;
            return cartItemToMergeWith;
        }
    }

    /**
     * Updates the cart items
     */
    private void updateCartItems (Id cartId, List<ccrz__E_CartItem__c> cartItems, ARC_DTO_Order.Order order)
    {
        // delete existing content to re-create the cart content from scratch
        deleteAllPackagesFromCart(cartId);
        delete cartItems;
        cartItems.clear();

        Set<String> productIdentifiers = new Set<String>();
        for (ARC_DTO_Order.OrderItem orderItem : order.items) {
            for (ARC_DTO_Order.OrderLine orderLine : orderItem.lines) {
                if (orderLine.eanOrUpc != null) {
                    productIdentifiers.add(orderLine.eanOrUpc);

                    // also add zero padded codes because SAP may strip leading zeros
                    productIdentifiers.add(ARC_Utilities.leftPadWithZeros(orderLine.eanOrUpc, EAN_CODE_LENGTH));
                    productIdentifiers.add(ARC_Utilities.leftPadWithZeros(orderLine.eanOrUpc, UPC_CODE_LENGTH));
                }
            }
        }
        List<ccrz__E_Product__c> products = productService.getProductsForEANorUPC(productIdentifiers);
        Map<String, ccrz__E_Product__c> eanOrUpcToProductMap = new Map<String, ccrz__E_Product__c>();
        for (ccrz__E_Product__c product : products) {
            if (product.EAN__c != null) {
                eanOrUpcToProductMap.put(product.EAN__c, product);
                eanOrUpcToProductMap.put(ARC_Utilities.stripLeadingZeros(product.EAN__c), product); // note: SAP may strip leading zeros
            }
            if (product.UPC__c != null) {
                eanOrUpcToProductMap.put(product.UPC__c, product);
                eanOrUpcToProductMap.put(ARC_Utilities.stripLeadingZeros(product.UPC__c), product); // note: SAP may strip leading zeros
            }
        }

        for (ARC_DTO_Order.OrderItem orderItem : order.items) {
            for (ARC_DTO_Order.OrderLine orderLine : orderItem.lines) {
                ccrz__E_Product__c product = eanOrUpcToProductMap.get(orderLine.eanOrUpc);
                if (orderLine.quantity != null && orderLine.quantity > 0 && product != null) {
                    ccrz__E_CartItem__c cartItem = new ccrz__E_CartItem__c();
                    cartItem.ccrz__CartItemType__c = 'Minor';
                    cartItem.ccrz__Product__c = product.Id;
                    cartItem.ccrz__ProductType__c = 'Product';
                    cartItem.ccrz__PricingType__c = cartConfiguration.getMinorCartItemPricingTypeExternal();
	                cartItem.ccrz__Quantity__c = orderLine.quantity;
	                cartItem.ccrz__SubAmount__c = orderLine.lineNetTotal;
	                cartItem.ccrz__RequestDate__c = orderLine.deliveryDate;
	                cartItem.ccrz__StoreId__c = order.storefront;
	                cartItem.ccrz__Cart__c = cartId;
	                if (orderLine.lineNetTotal != null) {
	                    cartItem.ccrz__Price__c = orderLine.lineNetTotal / orderLine.quantity;
	                }
	                mergeCartItemIntoList(cartItems, cartItem);
                }
            }
        }
        insert cartItems;
    }

    public void updateOrderComment (String encryptedCartId, Id orderCommentId)
    {
        ccrz__E_Cart__c cart = [
            SELECT
                Id,
                OrderComment__c
            FROM
                ccrz__E_Cart__c
            WHERE
                ccrz__EncryptedId__c =: encryptedCartId
            LIMIT
                1
        ];

        cart.OrderComment__c = orderCommentId;
        update cart;
    }

    //Update Selected ShipTo on Cart
    public void updateShipTo(String encryptedCartId, String selectedShipTo){
        try {
            ccrz__E_Cart__c currentCart = [
                SELECT Id, ShipTo__c, ccrz__EncryptedId__c
                FROM ccrz__E_Cart__c
                WHERE ccrz__EncryptedId__c = :encryptedCartId
                LIMIT 1
            ];
            if (currentCart.ShipTo__c != Id.valueof(selectedShipTo)) {
                currentCart.ShipTo__c = Id.valueof(selectedShipTo);
                update currentCart;
            }
        } catch (Exception e) {
            // TODO Call errorService to log error
            System.debug(e.getMessage());
        }
    }

    public Account getShipToDetails(String encryptedCartId){
        try {
            return [
                SELECT
                    AccountNumber,
                    Id,
                    Name,
                    ShippingCity,
                    ShippingPostalCode,
                    ShippingStreet,
                    EUID__c
                FROM
                    Account
                WHERE
                    Id IN (
                        SELECT
                            ShipTo__c
                        FROM
                            ccrz__E_Cart__c
                        WHERE
                            ccrz__EncryptedId__c = :encryptedCartId )
            ];
        } catch (QueryException e) {
            // TODO Call errorService to log error
            System.debug(e.getMessage());
            return null;
        }
    }

    public ccrz__E_Cart__c getCart (String encryptedCartId)
    {
        try {
            return [
                SELECT
                    ccrz__Account__c,
                    ccrz__Account__r.AccountNumber,
                    ccrz__EncryptedId__c,
                    ccrz__Name__c,
                    ccrz__Storefront__c,
                    Id,
                    OrderSimulationRunningSince__c,
                    SalesOrg__r.EUID__c,
                    ShipTo__c,
                    ShipTo__r.AccountNumber,
                    Name,
                    ccrz__CartType__c
                FROM
                    ccrz__E_Cart__c
                WHERE
                    ccrz__EncryptedId__c = :encryptedCartId
            ];
        } catch (QueryException e) {
            System.debug(e.getMessage());
            return null;
        }
    }

    public ccrz__E_Cart__c getCartById (String cartId)
    {
        try {
            return [
                SELECT
                    ccrz__Account__c,
                    ccrz__Account__r.AccountNumber,
                    ccrz__EncryptedId__c,
                    ccrz__Name__c,
                    ccrz__Storefront__c,
                    Id,
                    OrderSimulationRunningSince__c,
                    SalesOrg__r.EUID__c,
                    ShipTo__c,
                    ShipTo__r.AccountNumber,
                    Name,
                    ccrz__CartType__c
                FROM
                    ccrz__E_Cart__c
                WHERE
                    Id = :cartId
            ];
        } catch (QueryException e) {
            System.debug(e.getMessage());
            return null;
        }
    }

    public Boolean removeAllCartItems(ccrz__E_Cart__c cartWithItems) {
        try {
            List<Database.DeleteResult> deleteResults = Database.delete(cartWithItems.ccrz__E_CartItems__r, true);
            return true;
        } catch(Exception e) {
            errorService.log(ARC_Error.Scope.Application, ARC_Error.Code.E7100, e);
            return false;
        }
    }

    public Boolean setDefaultCartName(ccrz__E_Cart__c cart) {
        try {
            cart.ccrz__Name__c = i18nL18nService.getPageLabel(
                    userService.getCurrentUser().LocaleSidKey,
                    cart.ccrz__Storefront__c,
                    'Cart',
                    'ARC_your_personal_refnr'
            );
            if(cart.ccrz__Name__c == null)
                cart.ccrz__Name__c = defaultCartName;
            update cart;
            return true;
        } catch(Exception e) {
            errorService.log(ARC_Error.Scope.Application, ARC_Error.Code.E7000, e);
            return false;
        }
    }

    public Integer countProducts (String encryptedCartId)
    {
        AggregateResult result = [
            SELECT
                COUNT(Id) productCount
            FROM
                ccrz__E_CartItem__c
            WHERE
            ccrz__Cart__r.ccrz__EncryptedId__c = :encryptedCartId
            AND ccrz__ProductType__c = :ARC_Constants.PRODUCT_TYPE_SKU
        ];

        return (Integer) result.get('productCount');
    }

    public Integer getCartSize (String encryptedCartId)
    {
        AggregateResult result = [
            SELECT
                COUNT(Id) cartSize
            FROM
                ccrz__E_CartItem__c
            WHERE
            ccrz__Cart__r.ccrz__EncryptedId__c = :encryptedCartId
        ];

        return (Integer) result.get('cartSize');
    }


    public class CartException extends Exception
    {
    }

    public ccrz__E_Cart__c queryCartWithItems(String encryptedCartId) {
        ccrz__E_Cart__c cart = null;
		List<ccrz__E_Cart__c> cartList = [
			SELECT
                Id,
                Name,
                ccrz__Name__c,
                ccrz__Storefront__c,
                ccrz__ActiveCart__c,
                ccrz__User__c,
                ccrz__EncryptedId__c,
                ccrz__CartType__c,
                (
                    SELECT
                        Id,
                        groupId__c,
                        ccrz__ItemTotal__c,
                        ccrz__Product__c,
                        ccrz__Product__r.ccrz__ParentProduct__c,
                        ccrz__ProductType__c,
                        ccrz__Quantity__c,
                        ccrz__RequestDate__c
                    FROM
                        ccrz__E_CartItems__r
                )
            FROM
                ccrz__E_Cart__c
            WHERE
                ccrz__EncryptedId__c = :encryptedCartId
                OR Id = :encryptedCartId
            ORDER BY
                Id DESC
            LIMIT
                1
        ];

        if (cartList.size() > 0) {
            cart = cartList.get(0);
        }

        return cart;
    }

    // Queries for active carts of the current user in given storefront
    public List<ccrz__E_Cart__c> queryActiveCartsByStorefront(String storefrontName) {
        return [SELECT
            Id,
            Name,
            ccrz__Name__c,
            ccrz__CartType__c,
            ccrz__Storefront__c,
            ccrz__ActiveCart__c,
            ccrz__EncryptedId__c,
            ccrz__User__c
        FROM
            ccrz__E_Cart__c
        WHERE
        ccrz__User__c = :Userinfo.getUserId()
        AND ccrz__ActiveCart__c = TRUE
        AND ccrz__Storefront__c = :storefrontName
        ORDER BY
            Id DESC];
    }

    /*
        Check whether the quantity of the updated product is higher than the allowed limit which is configured with a custom setting.
        This only needs to be checked in case the account of the linked cart has been marked as "QuantityLimited".
     */
    public Boolean isQuantityLimitExceeded(String encryptedCartId, String prodId, Map<String, Id> productToParentProductIdMap, Map<Id, Double> parentProductToQuantitiesMap, Map<String, List<Double>> incomingQuantitiesMap)
    {
        ccrz__E_Cart__c cart = [
            SELECT
                ccrz__Account__c,
                ccrz__CartType__c
            FROM
                ccrz__E_Cart__c
            WHERE
                ccrz__EncryptedId__c = :encryptedCartId
            LIMIT
                1];

        // check for cart type as there is no limit for whishlists
        if (cart.ccrz__CartType__c == ARC_Dependencies.container.getConfiguration().getCartConfiguration().getCartTypeWishlist()) {
            return false;
        }

        Id cartAccount = cart.ccrz__Account__c;

        ARC_Service_Account accountService = ARC_Dependencies.container.getAccountService();
        Boolean isArticleQuantityLimited = accountService.getIsReorderArticleQuantityLimited(cartAccount);
        if ( !isArticleQuantityLimited ) {
            return false;
        }

        Id parentProd = productToParentProductIdMap.get(prodId);
        Double prodQuantity = parentProductToQuantitiesMap.get(parentProd);
        if ( prodQuantity == null ) {
            prodQuantity = 0;
        }
        prodQuantity += incomingQuantitiesMap.get(prodId).get(0);

        ARC_Configuration_User configurationUser = ARC_Dependencies.container.getConfiguration().getUserConfiguration();
        Double quantityLimit = Double.valueOf(configurationUser.getReorderQuantityPerArticleLimit());

        if ( prodQuantity > quantityLimit ) {
            return true;
        }

        return false;
    }

    public Boolean isQuantityLimitExceeded(ccrz__E_Cart__c cart, Boolean isArticleQuantityLimited, String prodId, Map<String, Id> productToParentProductIdMap, Map<Id, Double> parentProductToQuantitiesMap, Map<String, List<Double>> incomingQuantitiesMap) {
        // check for cart type as there is no limit for whishlists
        if (cart.ccrz__CartType__c == ARC_Dependencies.container.getConfiguration().getCartConfiguration().getCartTypeWishlist()) {
            return false;
        }

        Id cartAccount = cart.ccrz__Account__c;

        if (!isArticleQuantityLimited) {
            return false;
        }

        Id parentProd = productToParentProductIdMap.get(prodId);
        Double prodQuantity = parentProductToQuantitiesMap.get(parentProd);
        if (prodQuantity == null) {
            prodQuantity = 0;
        }
        prodQuantity += incomingQuantitiesMap.get(prodId).get(0);

        ARC_Configuration_User configurationUser = ARC_Dependencies.container.getConfiguration().getUserConfiguration();
        Double quantityLimit = Double.valueOf(configurationUser.getReorderQuantityPerArticleLimit());

        return prodQuantity > quantityLimit;
    }

    public Account getSoldTo(ccrz__E_Cart__c cart){
		ccrz__E_Cart__c accountCart = [
			SELECT
                        ccrz__Account__c
                    From
                        ccrz__E_Cart__c
                    WHERE
                        Id = :cart.Id];
        return accountService.getSoldTo(accountCart.ccrz__Account__c);
    }

    public Boolean removeProductsFromCart(Id userId, String encryptedCartId, List<Id> productIds, Id groupId) {
        ccrz__E_Cart__c cart = this.queryCartWithItems(encryptedCartId);

        Map<Id, ccrz__E_CartItem__c> cartItemsToRemove = new Map<Id, ccrz__E_CartItem__c>();

        for (ccrz__E_CartItem__c cartItem : cart.ccrz__E_CartItems__r) {
            for(Id productId : productIds) {
                if((cartItem.GroupId__c == groupId ) && (cartItem.ccrz__Product__c == productId || cartItem.ccrz__Product__r.ccrz__ParentProduct__c == productId)) {
                    if(!(cartItemsToRemove.containsKey(cartItem.Id))) {
                        cartItemsToRemove.put(cartItem.Id, cartItem);
                    }
                }
            }
        }
        return deleteCartItems(cartItemsToRemove.values());
    }

    public Boolean removeProductsFromCart(Id user ,String encryptedCartId, List<Id> productIds) {

        return removeProductsFromCart(user,encryptedCartId,productIds,null);
    }

    private Boolean deleteCartItems( List<ccrz__E_CartItem__c> cartItemsToRemove) {
        try {

            delete cartItemsToRemove;

        } catch (QueryException e) {

            System.debug(e.getMessage());
            return false;

        }
        return true;
    }

    public List<ccrz__E_CartItem__c> getCartItemsByCartIdAndGroupId(String encryptedCartId, String groupId) {
        return [
            SELECT
                Id,
                GroupId__c,
                ccrz__Product__c,
                Name,
                ccrz__Quantity__c,
                ccrz__RequestDate__c,
                ccrz__Product__r.ccrz__ProductType__c,
                ccrz__Cart__r.ccrz__EncryptedId__c,
                ccrz__Cart__c,
                ccrz__Product__r.ccrz__ParentProduct__c
            FROM
                ccrz__E_CartItem__c
            WHERE
                ccrz__Cart__r.ccrz__EncryptedId__c = :encryptedCartId
                AND GroupId__c = :groupId
            ORDER BY
                ccrz__RequestDate__c ASC
        ];
    }

    public List<ccrz__E_CartItem__c> getCartItemsByParentProductIds(Id cartId, Set<Id> singleAggregatedProducts) {
        return [
            SELECT
                Id,
                GroupId__c,
                Name,
                ccrz__Cart__c,
                ccrz__Product__c,
                ccrz__Product__r.ccrz__ParentProduct__c,
                ccrz__Product__r.ccrz__ProductType__c
            FROM
                ccrz__E_CartItem__c
            WHERE
                ccrz__Product__r.ccrz__ParentProduct__c IN :singleAggregatedProducts
                AND	ccrz__Cart__c = :cartId
        ];
    }

    public List<ARC_DTO_CartItems> getExtraInformationCartItems (String encryptedCartId)
    {

        List<ARC_DTO_CartItems> cartItemsDTOs = new List<ARC_DTO_CartItems>();

        ccrz__E_Cart__c cart = this.queryCartWithItems(encryptedCartId);

        for (ccrz__E_CartItem__c cartItem : cart.ccrz__E_CartItems__r) {
            if (cartItem.ccrz__ProductType__c == ARC_Constants.PRODUCT_TYPE_EQUALS_PRODUCT) {
                ARC_DTO_CartItems cartItemDTO = new ARC_DTO_CartItems();
                cartItemDTO.cartItemId = cartItem.Id;
                cartItemDTO.requestDate = String.valueOf(cartItem.ccrz__RequestDate__c);
                cartItemsDTOs.add(cartItemDTO);
            }
        }

        return cartItemsDTOs;
    }

    public ccrz__E_Cart__c getActiveCart (User user, String storefront)
    {
        return this.cartDataGateway.getActive(user.Id, storefront);
    }

    public List<ccrz__E_Cart__c> getWishlists (String storefront)
    {
        return this.cartDataGateway.getWishlistsWithItems(storefront);
    }

    public List<ccrz__E_Cart__c> createCarts (List<Map<String, Object>> cartList)
    {
        Map<String,Object> createResults = ccrz.ccAPICart.create(
            new Map<String,Object>{
                ccrz.ccApi.API_VERSION => 4,
                ccrz.ccApiCart.CART_OBJLIST => cartList,
                ccrz.ccAPI.SIZING => new Map<String, Object>{
                    ccrz.ccAPICart.ENTITYNAME => new Map<String, Object>{
                        ccrz.ccAPI.SZ_REFETCH => true,
                        ccrz.ccAPI.SZ_SKIPTRZ => true
                    }
                }
            }
        );

        return (List<ccrz__E_Cart__c>)createResults.get(ccrz.ccApiCart.CART_OBJLIST);
    }

    public Map<String, Object> addToCart(Set<String> skus, String encryptedCartId)
    {
        Map<String, Object> results = ccrz.ccApiCart.addTo(
            new Map<String,Object>{
                ccrz.ccApi.API_VERSION => 1,
                ccrz.ccApiCart.CART_ENCID => encryptedCartId,
                'skus' => skus
            }
        );

        return results;
    }

    public Map<String, Object> addToCart(List<ccrz__E_CartItem__c> cartItems, String encryptedCartId)
    {
        Map<String, Object> results = ccrz.ccApiCart.addTo(
            new Map<String,Object>{
                ccrz.ccApi.API_VERSION => 1,
                ccrz.ccApiCart.CART_ENCID => encryptedCartId,
                'cartItems' => cartItems
            }
        );

        return results;
    }

    public ccrz__E_Cart__c cloneCartWithItems(String cartId) {
        ccrz__E_Cart__c parentCartCloned = cloneCart(cartId);
        if (parentCartCloned != null && cloneCartItems(cartId, parentCartCloned.Id)) {
            return parentCartCloned;
        }
        return null;
    }

    private ccrz__E_Cart__c cloneCart(String cartId) {

        String soql = ARC_Utilities.getCreatableFieldsSOQL('ccrz__E_Cart__c', 'id=\'' + cartId + '\'');

        ccrz__E_Cart__c originCart = (ccrz__E_Cart__c) Database.query(soql);
        ccrz__E_Cart__c cloneCart = originCart.clone(false, true);

        cloneCart.ccrz__EncryptedId__c = ARC_Utilities.newGUID();
        cloneCart.ccrz__CartId__c = null;
        cloneCart.ccrz__RequestDate__c = null;
        cloneCart.ccrz__CartStatus__c = 'Open';
        cloneCart.ShipTo__c = null;
        cloneCart.OrderComment__c = null;
        cloneCart.OrderSimulationRunningSince__c = null;
        cloneCart.ccrz__Name__c = cloneCart.ccrz__Storefront__c + ' ' + System.now();
        cloneCart.ccrz__ActiveCart__c = TRUE;

        try {
            insert cloneCart;
        } catch (DmlException e) {
            System.debug('\nException inserting cloned cart: ' + e.getMessage());
        }

        return cloneCart;
    }

    private Boolean cloneCartItems(Id cartId, Id cartIdCloned) {

        String soqlCartItem = ARC_Utilities.getCreatableFieldsSOQL('ccrz__E_CartItem__c', 'ccrz__Cart__c=\'' + cartId + '\' ');
        List<ccrz__E_CartItem__c> originCartItems = (List<ccrz__E_CartItem__c>) Database.query(soqlCartItem);

        map<Id, ccrz__E_CartItem__c> relatedCloneToParent = new map<Id, ccrz__E_CartItem__c>();
        List<ccrz__E_CartItem__c> aggregatedCartItems = new List<ccrz__E_CartItem__c>();
        List<ccrz__E_CartItem__c> productCartItems = new List<ccrz__E_CartItem__c>();


        for (ccrz__E_CartItem__c originCartItem : originCartItems) {
            if (originCartItem.ccrz__ProductType__c == ARC_Constants.PRODUCT_TYPE_SKU) {
                ccrz__E_CartItem__c cloneCartItem = originCartItem.clone(false, true);
                cloneCartItem.ccrz__Cart__c = cartIdCloned;
                cloneCartItem.ccrz__CartItemId__c = null;
                relatedCloneToParent.put(originCartItem.Id, cloneCartItem);
                aggregatedCartItems.add(cloneCartItem);
            }
        }

        try {
            insert aggregatedCartItems;
        } catch (DmlException e) {
            System.debug('\nException cloning aggregateType cartitem: ' + e.getMessage());
            return false;
        }

        for (ccrz__E_CartItem__c originCartItem : originCartItems) {
            if (originCartItem.ccrz__ProductType__c == ARC_Constants.PRODUCT_TYPE_EAN) {
                ccrz__E_CartItem__c cloneCartItem = originCartItem.clone(false, true);
                cloneCartItem.ccrz__Cart__c = cartIdCloned;
                cloneCartItem.ccrz__ParentCartItem__c = relatedCloneToParent.get(originCartItem.ccrz__ParentCartItem__c).Id;
                productCartItems.add(cloneCartItem);
            }
        }

        try {
            insert productCartItems;
        } catch (DmlException e) {
            System.debug('\nException cloning productType cartitem: ' + e.getMessage());
            return false;
        }

        return true;
    }

    public List<ccrz__E_CartItem__c> getShifted(List<ccrz__E_CartItem__c> cartItems)
    {
        Map<String, ccrz__E_CartItem__c> skuToCartItem = new Map<String, ccrz__E_CartItem__c>();

        for (ccrz__E_CartItem__c ci: cartItems) {

            if (ci.ccrz__RequestDate__c >= Date.today()
                || ci.ccrz__CartItemType__c == this.cartConfiguration.getMajorCartItemType()
            ) {
                continue;
            }

            String cartId = ci.ccrz__Cart__r.ccrz__CartId__c;
            System.debug(cartid);

            String sizeLevelSku = ci.ccrz__Product__r.EUID__c;

            ccrz__E_CartItem__c todaysItem = skuToCartItem.get(sizeLevelSku);
            if (todaysItem == null) {
                todaysItem = new ccrz__E_CartItem__c(
                    ccrz__Cart__r = new ccrz__E_Cart__c(ccrz__CartId__c = cartId),
                    ccrz__CartItemId__c = cartId + '-' + sizeLevelSku + '-' + ((DateTime)Date.today()).format('yyyy-MM-dd'),
                    ccrz__cartItemType__c = cartConfiguration.getMinorCartItemType(),
                    ccrz__Price__c = 0.0,
                    ccrz__Product__r = new ccrz__E_Product__c(EUID__c = sizeLevelSku),
                    ccrz__Quantity__c = 0.0,
                    ccrz__RequestDate__c = Date.today(),
                    ccrz__SubAmount__c = 0.0,
                    ccrz__ItemStatus__c = String.valueOf(ARC_CartItemStatus.SHIFTED)
                );
            }

            todaysItem.ccrz__Quantity__c += ci.ccrz__Quantity__c;
            skuToCartItem.put(sizeLevelSku, todaysItem);
            ci.ccrz__ItemStatus__c = String.valueOf(ARC_CartItemStatus.REMOVE_AFTER_SHIFT);
            ci.ccrz__Quantity__c = 0.0;
        }

        List<ccrz__E_CartItem__c> shifted = skuToCartItem.values();
        shifted.addAll(cartItems);

        return shifted;
    }

    public void checkAvailability(Decimal availableInventory, ccrz__E_CartItem__c cartItem)
    {
        if (cartItem.ccrz__ItemStatus__c == String.valueOf(ARC_CartItemStatus.SHIFTED)
            || cartItem.ccrz__ItemStatus__c == String.valueOf(ARC_CartItemStatus.REMOVE_AFTER_SHIFT)) {
            return;
        }

        if (cartItem.ccrz__Quantity__c == 0) {

            cartItem.ccrz__Quantity__c = 0;
            cartItem.ccrz__ItemStatus__c = String.valueOf(ARC_CartItemStatus.REMOVED);

        } else if (availableInventory <= 0) {

            cartItem.ccrz__Quantity__c = 0;
            cartItem.ccrz__ItemStatus__c = String.valueOf(ARC_CartItemStatus.INVALID);

        } else if (availableInventory > 0 && availableInventory >= cartItem.ccrz__Quantity__c) {

            cartItem.ccrz__ItemStatus__c = String.valueOf(ARC_CartItemStatus.MODIFIED);

        } else if (availableInventory > 0 && availableInventory < cartItem.ccrz__Quantity__c) {

            cartItem.ccrz__Quantity__c = availableInventory;
            cartItem.ccrz__ItemStatus__c = String.valueOf(ARC_CartItemStatus.ADJUSTED);
        }
    }

}