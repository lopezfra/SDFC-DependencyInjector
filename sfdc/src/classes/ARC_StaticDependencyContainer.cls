/**
 * Resolving dependencies by storing the instances in heap memory. Evaluate serialization later. Introduce abstract then.
 */
public with sharing class ARC_StaticDependencyContainer implements ARC_DependencyContainer
{
    /**
     * The heap registry. Due to limitations requesting a non instanciated class name, it can be as well as defined
     * directly in the dependency providing method.
     */
    public static Map<String, Object> registry = new Map<String, Object>();

    public ARC_Service_Assortment getAssortmentService ()
    {
        String dependencyKey = 'assortmentService';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Service_Assortment) getDependency(dependencyKey);
        }

        ARC_Service_Assortment dependency = new ARC_Service_CcrzAssortment();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    public ARC_Service_CatalogueItem getCatalogueItemService() {

        String dependencyKey = 'catalogueItemService';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Service_CatalogueItem) getDependency(dependencyKey);
        }

        ARC_Service_CatalogueItem dependency = new ARC_Service_CatalogueProducts();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    public ARC_Service_Category getCategoryService ()
    {
        String dependencyKey = 'categoryService';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Service_Category) getDependency(dependencyKey);
        }

        ARC_Service_Category dependency = new ARC_Service_CcrzCategory(
            this.getCategoryDataGateway(),
            this.getUserService(),
            this.getSalesOrgService()
        );
        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    public ARC_Service_CategoryMedia getCategoryMediaService() {
        String dependencyKey = 'categoryMediaService';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Service_CategoryMedia) getDependency(dependencyKey);
        }

        ARC_Service_CategoryMedia dependency = new ARC_Service_CcrzCategoryMedia();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }

    public ARC_Configuration getConfiguration ()
    {
        String dependencyKey = 'configuration';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Configuration) getDependency(dependencyKey);
        }

        ARC_Configuration dependency = new ARC_LocalConfiguration();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    public ARC_Service_Colors getColorService ()
    {
        String dependencyKey = 'colorService';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Service_Colors) getDependency(dependencyKey);
        }

        ARC_Service_Colors dependency = new ARC_Service_Colors();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    public ARC_Service_DeliveryBlock getDeliveryBlockService ()
    {
        String dependencyKey = 'deliveryBlockService';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Service_DeliveryBlock) getDependency(dependencyKey);
        }

        ARC_Service_DeliveryBlock dependency = new ARC_Service_SalesOrgDeliveryBlock();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }

    public ARC_Service_OrderBlock getOrderBlockService ()
    {
        String dependencyKey = 'orderBlockService';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Service_OrderBlock) getDependency(dependencyKey);
        }

        ARC_Service_OrderBlock dependency = new ARC_Service_SalesOrgOrderBlock();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }

    public ARC_Service_Error getErrorService ()
    {
        String dependencyKey = 'errorService';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Service_Error) getDependency(dependencyKey);
        }

        ARC_Service_Error dependency = new ARC_Service_ErrorLog();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    public ARC_Service_Listing getListingService ()
    {
        String dependencyKey = 'listingService';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Service_Listing) getDependency(dependencyKey);
        }

        ARC_Service_Listing dependency = new ARC_Service_LeTableListing(
            this.getConfiguration().getListingConfiguration()
        );
        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    public ARC_Service_i18nL18n geti18nL18nService ()
    {
        String dependencyKey = 'i18nL18nService';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Service_i18nL18n) getDependency(dependencyKey);
        }

        ARC_Service_i18nL18n dependency = new ARC_Service_Globali18nL18n(
            this.getErrorService()
        );
        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    public ARC_Service_Account getAccountService ()
    {
        String dependencyKey = 'accountService';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Service_Account) getDependency(dependencyKey);
        }

        ARC_Service_Account dependency = new ARC_Service_SfdcAccount(
            this.getAccountDataGateway(),
            this.getUserDataGateway(),
            this.getSalesOrgService(),
            this.getOrderBlockService()
        );
        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    public ARC_Service_ProductCategory getProductCategoryService ()
    {
        String dependencyKey = 'productCategoryService';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Service_ProductCategory) getDependency(dependencyKey);
        }

        ARC_Service_ProductCategory dependency = new ARC_Service_ProductGroup();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    public ARC_Service_Message getMessageService ()
    {
        String dependencyKey = 'messageService';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Service_Message) getDependency(dependencyKey);
        }

        ARC_Service_Message dependency = new ARC_Service_GlobalMessage();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }

    public ARC_Service_Package getPackageService ()
    {
        String dependencyKey = 'packageService';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Service_Package) getDependency(dependencyKey);
        }

        ARC_Service_Package dependency = new ARC_Service_LocalPackage();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }

    public ARC_Service_Pricing getPricingService ()
    {
        String dependencyKey = 'pricingService';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Service_Pricing) getDependency(dependencyKey);
        }

        ARC_Service_Pricing dependency = new ARC_Service_CcrzPricing();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    public ARC_Service_ProductDetails getProductDetailsService ()
    {
        String dependencyKey = 'productDetailsService';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Service_ProductDetails) getDependency(dependencyKey);
        }

        ARC_Service_ProductDetails dependency = new ARC_Service_LeTableProductDetails();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    public ARC_Service_ProductList getProductListService ()
    {
        String dependencyKey = 'productListService';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Service_ProductList) getDependency(dependencyKey);
        }

        ARC_Service_ProductList dependency = new ARC_Service_ProductList(
            this.getConfiguration().getProductConfiguration()
        );
        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    public ARC_Service_Product getProductService ()
    {
        String dependencyKey = 'productService';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Service_Product) getDependency(dependencyKey);
        }

        ARC_Configuration configuration = this.getConfiguration();
        ARC_Service_Product dependency = new ARC_Service_CcrzProduct(
            configuration.getProductConfiguration(),
            this.getResourceService(),
            this.getProductFilterService(),
            this.getSalesOrgService(),
            this.getStorefrontService(),
            this.getGlobalCacheService(),
            this.getProductDataGateway()
        );
        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    public ARC_Service_ProductRecommendationEngine getProductRecommendationEngine ()
    {
        String dependencyKey = 'productRecommendationEngine';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Service_ProductRecommendationEngine) getDependency(dependencyKey);
        }

        ARC_Service_ProductRecommendationEngine dependency = new ARC_Service_SimilarProdRecEngine();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    public ARC_Service_ProductStatus getProductStatusService ()
    {
        String dependencyKey = 'productStatusService';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Service_ProductStatus) getDependency(dependencyKey);
        }

        ARC_Service_ProductStatus dependency = new ARC_Service_SoldOutStatus(this.getProductStatusDataGateway());
        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    public ARC_Service_ProductFilter getProductFilterService ()
    {
        String dependencyKey = 'productFilterService';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Service_ProductFilter) getDependency(dependencyKey);
        }

        ARC_Service_ProductFilter dependency = new ARC_Service_CcrzProductFilter();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    public ARC_Service_Sizes getSizeService ()
    {
        String dependencyKey = 'sizeService';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Service_Sizes) getDependency(dependencyKey);
        }

        ARC_Service_Sizes dependency = new ARC_Service_ProductSizes(
            this.getConfiguration().getProductConfiguration(),
            this.getPricingService(),
            this.getAccountService(),
            this.getStorefrontService(),
            this.getSalesOrgService(),
            this.getCache(),
            this.getConfiguration().getStoreConfiguration(),
            this.getProductAvailabilityService()
        );
        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    public ARC_Service_SalesOrg getSalesOrgService ()
    {
        String dependencyKey = 'salesOrgService';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Service_SalesOrg) getDependency(dependencyKey);
        }

        ARC_Service_SalesOrg dependency = new ARC_Service_SalesOrg();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    public ARC_Service_Season getSeasonService ()
    {
        String dependencyKey = 'seasonService';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Service_Season) getDependency(dependencyKey);
        }

        ARC_Service_Season dependency = new ARC_Service_LocalSeason(
            this.getSeasonDataGateway(),
            this.getConfiguration().getStoreConfiguration(),
            this.getAccountService(),
            this.getSalesOrgService()
        );
        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    public ARC_Service_Security getSecurityService ()
    {
        String dependencyKey = 'securityService';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Service_Security) getDependency(dependencyKey);
        }

        ARC_Service_Security dependency = new ARC_Service_PortalSecurity();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    public ARC_Service_Spec getSpecService ()
    {
        String dependencyKey = 'specService';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Service_Spec) getDependency(dependencyKey);
        }

        ARC_Service_Spec dependency = new ARC_Service_CcrzSpec();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    public ARC_Service_Storefront getStorefrontService ()
    {
        String dependencyKey = 'storefrontService';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Service_Storefront) getDependency(dependencyKey);
        }

        ARC_Service_Storefront dependency = new ARC_Service_CcrzStorefront();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    public ARC_Service_ProductAvailability getProductAvailabilityService ()
    {
        String dependencyKey = 'productAvailabilityService';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Service_ProductAvailability) getDependency(dependencyKey);
        }

        ARC_Service_ProductAvailability dependency = new ARC_Service_ProductAvailabilityDate();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    public ARC_Service_Order getOrderService ()
    {
        String dependencyKey = 'orderService';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Service_Order) getDependency(dependencyKey);
        }

        ARC_Service_Order dependency = new ARC_Service_CcrzOrder();

        registerDependency(dependencyKey, dependency);

        return dependency;
    }

    public ARC_Service_OrderSubmission getOrderSubmissionService ()
    {
        String dependencyKey = 'orderSubmissionService';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Service_OrderSubmission) getDependency(dependencyKey);
        }

        ARC_Service_OrderSubmission dependency = new ARC_Service_CcrzOrderSubmission(
                this.getConfiguration().getOrderConfiguration()
        );

        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    public ARC_Service_Contact getContactService ()
    {
        String dependencyKey = 'contactService';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Service_Contact) getDependency(dependencyKey);
        }

        ARC_Service_Contact dependency = new ARC_Service_LocalContact(
            this.getContactDataGateway(),
            this.getConfiguration().getUserConfiguration()
        );
        registerDependency(dependencyKey, dependency);

        return dependency;
    }

    public ARC_API_Order getOrderAPI ()
    {
        String dependencyKey = 'orderAPI';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_API_Order) getDependency(dependencyKey);
        }

        ARC_API_Order dependency = new ARC_API_OrderFactory().create();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }

    public ARC_Service_Cart getCartService ()
    {
        String dependencyKey = 'cartService';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Service_Cart) getDependency(dependencyKey);
        }

        ARC_Service_Cart dependency = new ARC_Service_CcrzCart();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }

    public ARC_Service_User getUserService() {
        String dependencyKey = 'userService';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Service_User) getDependency(dependencyKey);
        }

        ARC_Service_User dependency = new ARC_Service_PortalUser(
            this.getUserDataGateway(),
            this.getErrorService(),
            this.getCache()
        );
        registerDependency(dependencyKey, dependency);

        return dependency;
    }

    public ARC_Service_ContactAddress getContactAddresssService ()
    {
        String dependencyKey = 'ContactAddressService';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Service_ContactAddress) getDependency(dependencyKey);
        }

        ARC_Service_ContactAddress dependency = new ARC_Service_CcrzContactAddress();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }

    public ARC_Service_ContentPage getContentPageService ()
    {
        String dependencyKey = 'contentPageService';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Service_ContentPage) getDependency(dependencyKey);
        }

        ARC_Service_ContentPage dependency = new ARC_Service_LocalContentPage();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }

    public ARC_Cache getCache ()
    {
        String dependencyKey = 'cache';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Cache) getDependency(dependencyKey);
        }

        ARC_Cache dependency = new ARC_SessionCache();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }

    public ARC_Cache getGlobalCacheService ()
    {
        String dependencyKey = 'globalCache';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Cache) getDependency(dependencyKey);
        }

        ARC_Cache dependency = new ARC_Service_OrgCache();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }

    public ARC_Service_Resource getResourceService ()
    {
        String dependencyKey = 'resourceService';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_Service_Resource) getDependency(dependencyKey);
        }

        ARC_Service_Resource dependency = new ARC_Service_StaticResource();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    public ARC_TDGW_Account getAccountDataGateway ()
    {
        String dependencyKey = 'accountDataTableGateway';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_TDGW_Account) getDependency(dependencyKey);
        }

        ARC_TDGW_Account dependency = new ARC_TDGW_AccountSObject();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    public ARC_TDGW_Cart getCartDataGateway ()
    {
        String dependencyKey = 'cartDataGateway';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_TDGW_Cart) getDependency(dependencyKey);
        }

        ARC_TDGW_Cart dependency = new ARC_TDGW_CartSObject(
            this.getConfiguration().getCartConfiguration()
        );
        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    public ARC_TDGW_Category getCategoryDataGateway ()
    {
        String dependencyKey = 'categoryDataGateway';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_TDGW_Category) getDependency(dependencyKey);
        }

        ARC_TDGW_Category dependency = new ARC_TDGW_CategorySObject();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    public ARC_TDGW_Contact getContactDataGateway ()
    {
        String dependencyKey = 'contactDataTableGateway';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_TDGW_Contact) getDependency(dependencyKey);
        }

        ARC_TDGW_Contact dependency = new ARC_TDGW_ContactSObject();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    public ARC_TDGW_Inventory getInventoryDataGateway ()
    {
        String dependencyKey = 'inventoryDataGateway';

        if (isDependencyRegistered(dependencyKey)){
            return (ARC_TDGW_InventorySObject) getDependency(dependencyKey);
        }

        ARC_TDGW_InventorySObject dependency = new ARC_TDGW_InventorySObject();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    public ARC_TDGW_PriceList getPriceListDataGateway()
    {
        String dependencyKey = 'priceListDataGateway';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_TDGW_PriceList) getDependency(dependencyKey);
        }

        ARC_TDGW_PriceList dependency = new ARC_TDGW_PriceListSobject();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    public ARC_TDGW_PriceListItem getPriceListItemDataGateway()
    {
        String dependencyKey = 'priceListItemDataGateway';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_TDGW_PriceListItem) getDependency(dependencyKey);
        }

        ARC_TDGW_PriceListItem dependency = new ARC_TDGW_PriceListItemSObject();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    public ARC_TDGW_Product getProductDataGateway()
    {
        String dependencyKey = 'productDataGateway';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_TDGW_Product) getDependency(dependencyKey);
        }

        ARC_TDGW_Product dependency = new ARC_TDGW_ProductSObject(
            this.getConfiguration().getProductConfiguration()
        );
        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    public ARC_TDGW_ProductStatus getProductStatusDataGateway()
    {
        String dependencyKey = 'productStatusDataGateway';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_TDGW_ProductStatus) getDependency(dependencyKey);
        }

        ARC_TDGW_ProductStatus dependency = new ARC_TDGW_ProductStatusSObject();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    public ARC_TDGW_Season getSeasonDataGateway ()
    {
        String dependencyKey = 'seasonDataTableGateway';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_TDGW_Season) getDependency(dependencyKey);
        }

        ARC_TDGW_Season dependency = new ARC_TDGW_SeasonSObject();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    public ARC_TDGW_ProductStorefront getProductStorefrontDataGateway ()
    {
        String dependencyKey = 'productStorefrontDataGateway';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_TDGW_ProductStorefront) getDependency(dependencyKey);
        }

        ARC_TDGW_ProductStorefront dependency = new ARC_TDGW_ProductStorefrontSObject();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }

    public ARC_TDGW_User getUserDataGateway ()
    {
        String dependencyKey = 'userDataTableGateway';

        if (isDependencyRegistered(dependencyKey)) {
            return (ARC_TDGW_User) getDependency(dependencyKey);
        }

        ARC_TDGW_User dependency = new ARC_TDGW_UserSObject();
        registerDependency(dependencyKey, dependency);

        return dependency;
    }


    /**
    * Methods for dependency registration
    */
    private Boolean isDependencyRegistered (String dependencyKey)
    {
        return registry.containsKey(dependencyKey);
    }


    private Object getDependency (String dependencyKey)
    {
        return registry.get(dependencyKey);
    }


    private void registerDependency (String dependencyKey, Object dependency)
    {
        registry.put(dependencyKey, dependency);
    }
}