/**
 * Providing access to the dependency container
 */
public with sharing class ARC_Dependencies
{
    public static final ARC_DependencyContainer container = new ARC_StaticDependencyContainer();
}
