@isTest
private class ARC_Service_CcrzCartTest
{
    private static final Integer EAN_CODE_LENGTH = 13;

    private static final Integer UPC_CODE_LENGTH = 12;

    private static ccrz__E_Cart__c testCart;

    private static ccrz__E_Cart__C cart;

    private static User testPortalAccountOwner;

    private static String encryptedCartId = ARC_TestDataFactory.newGUID();

    private static Account defaultShipToAccount;

    private static Account shipToAccount;

    private static Contact contact;

    private static User testUser;

    private static ARC_Configuration_Cart cartConfiguration;

    private static ARC_TDGW_Cart cartDataGateway;

    private static ARC_Service_Account testAccountService;

    private static ARC_Service_Order orderService;

    private static ARC_Service_Error testErrorService;

    private static ARC_Service_Cart cartService;

    private static ccrz__E_product__c colorProduct;

    private static ARC_Service_Product testProductService;

    private static ARC_Service_ProductDetails testProductDetailsService;

    private static ARC_Service_i18nL18n testI18nL18nService;

    private static ARC_Service_User testUserService;

    private static fflib_ApexMocks mocks  = new fflib_ApexMocks();

    private static List<ccrz__E_CartItem__c> cartItems;


    private static void setUp()
    {
        testPortalAccountOwner = ARC_TestDataFactory.createPortalAccountOwner();
        insert testPortalAccountOwner;

        System.runAs(testPortalAccountOwner)
        {
            Account masterAccount = ARC_TestDataFactory.createAccount('Master Account', '0000000002');
            masterAccount.OwnerId = testPortalAccountOwner.Id;
            insert masterAccount;

            contact = ARC_TestDataFactory.createContact(masterAccount);
            insert contact;

            testUser = ARC_TestDataFactory.createPortalUser(contact, 'mail@service.de');
            insert testUser;

            shipToAccount = ARC_TestDataFactory.createAccount('Ship To Account', '0000000002');
            shipToAccount.OwnerId = testPortalAccountOwner.Id;
            insert shipToAccount;

            defaultShipToAccount = ARC_TestDataFactory.createAccount('Default Ship To Account', '0000000003');
            defaultShipToAccount.OwnerId = testPortalAccountOwner.Id;
            insert defaultShipToAccount;
        }

        System.runAs(testUser)
        {
            testCart = new ccrz__E_Cart__c(
                ccrz__Name__c = 'TestCart',
                ShipTo__c = defaultShipToAccount.Id,
                ccrz__EncryptedId__c = encryptedCartId
            );
            insert testCart;

            cart = ARC_TestDataFactory.createCart('MyCart',contact,shipToAccount);
            insert cart;

            insert new AdidaseTailingGlobalConfiguration__c(Name='IntegrationOrderImplementation',Value__c='Mock');

            cartService = ARC_Dependencies.container.getCartService();
        }
    }


    @IsTest
    private static void setUpMocks ()
    {
        mocks = new fflib_ApexMocks();
        cartConfiguration = new ARC_Mock_Configurations.ARC_Mock_CartConfiguration(mocks);
        cartDataGateway = new ARC_Mock_TableDataGateway.ARC_Mock_TDGW_Cart(mocks);
        testAccountService = new ARC_Mock_Service.ARC_Mock_Service_Account(mocks);
        orderService = new ARC_Mock_Service.ARC_Mock_Service_Order(mocks);
        testErrorService = new ARC_Mock_Service.ARC_Mock_Service_Error(mocks);
        testProductService = new ARC_Mock_Service.ARC_Mock_Service_Product(mocks);
        testProductDetailsService = new ARC_Mock_Service.ARC_Mock_Service_ProductDetails(mocks);
        testI18nL18nService = new ARC_Mock_Service.ARC_Mock_Service_i18nL18n(mocks);
        testUserService = new ARC_Mock_Service.ARC_Mock_Service_User(mocks);

        cartService = new ARC_Service_CcrzCart(
            cartConfiguration,
            cartDataGateway,
            ARC_Dependencies.container.getSalesOrgService(),
            testAccountService,
            orderService,
            testErrorService,
            testProductService,
            testProductDetailsService,
            testI18nL18nService,
            testUserService
        );
    }


    @isTest
    public static void testUpdateOrderComment()
    {
        setup();

        System.runAs(testUser) {
            SalesOrg__c salesOrg = ARC_TestDataFactory.createSalesOrg(
                'adidasGermany',
                '0100',
                'DE',
                '0100'
            );
            insert salesOrg;

            OrderComment__c orderComment = new OrderComment__c();
            orderComment.SalesOrg__c = salesOrg.Id;
            orderComment.Name = 'TestOrderComment';
            orderComment.IsActive__c = true;
            insert orderComment;

            Test.startTest();
            cartService.updateOrderComment(encryptedCartId, orderComment.Id);
            Test.stopTest();

            cart = [
                SELECT
                    OrderComment__c
                FROM
                    ccrz__E_Cart__c
                WHERE
                    ccrz__EncryptedId__c =: encryptedCartId
            ];

            System.assertEquals(orderComment.Id, cart.OrderComment__c);
        }
    }


    @isTest
    public static void testUpdateShipTo()
    {
        setUp();

        System.runAs(testUser)
        {
            Test.startTest();

            cartService.updateShipTo(encryptedCartId, shipToAccount.Id);

            Test.stopTest();
        }

        ccrz__E_Cart__c updatedCart = [
            SELECT ShipTo__c FROM ccrz__E_Cart__c
            WHERE ccrz__EncryptedId__c =: encryptedCartId
        ];

        System.assertEquals(shipToAccount.Id, updatedCart.ShipTo__c);
    }


    public static testMethod void testUpdateShipToWithInvalidParameter()
    {
        setUp();

        System.runAs(testUser){

            Test.startTest();

            cartService.updateShipTo('INVALID_ID', 'SOME SHIP TO ID');

            Test.stopTest();
        }

        ccrz__E_Cart__c updatedCart = [
            SELECT ShipTo__c FROM ccrz__E_Cart__c
            WHERE ccrz__EncryptedId__c =: encryptedCartId
        ];

        System.assertEquals(defaultShipToAccount.Id, updatedCart.ShipTo__c);
    }


    @IsTest
    private static void testGetShipTo ()
    {
        setUp();

        Account shipTo;

        System.runAs(testUser)
        {
            Test.startTest();

            shipTo = cartService.getShipToDetails(encryptedCartId);

            Test.stopTest();
        }

        System.assertEquals(defaultShipToAccount.AccountNumber, shipTo.AccountNumber);
        System.assertEquals(defaultShipToAccount.EUID__c, shipTo.EUID__c);
        System.assertEquals(defaultShipToAccount.Id, shipTo.Id);
        System.assertEquals(defaultShipToAccount.Name, shipTo.Name);
        System.assertEquals(defaultShipToAccount.ShippingCity, shipTo.ShippingCity);
        System.assertEquals(defaultShipToAccount.ShippingPostalCode, shipTo.ShippingPostalCode);
        System.assertEquals(defaultShipToAccount.ShippingStreet, shipTo.ShippingStreet);
    }


    public static testMethod void testGetShipToWithInvalidParameter()
    {
        setUp();

        Account shipTo;

        System.runAs(testUser)
        {
            Test.startTest();

            shipTo = cartService.getShipToDetails('INVALID_ID');

            Test.stopTest();
        }

        System.assertEquals(null, shipTo);
    }


    public static testMethod void testCountProducts ()
    {
        setUp();

        System.runAs(testPortalAccountOwner)
        {
            List<ccrz__E_Product__c> products = ARC_TestDataFactory.createProducts(3);
            products.get(1).ccrz__ProductType__c = ARC_Constants.PRODUCT_TYPE_EAN;
            insert products;

            insert ARC_TestDataFactory.createCartItemList(testCart, products);
        }

        System.runAs(testUser)
        {
            Test.startTest();

            Integer count = cartService.countProducts(encryptedCartId);

            Test.stopTest();

            System.assertEquals(2, count);
        }
    }


    public static testMethod void testCalculateNetPricesSucces ()
    {
        setUp();
        setUpMocks();

        SalesOrg__c salesOrg;
        System.runAs(testPortalAccountOwner)
        {
             salesOrg = ARC_TestDataFactory.createSalesOrg('SalesOrgName', 'adidas', 'Germany', '345678567565');
            INSERT salesOrg;

            AccountSalesOrg__c accountSalesOrg = ARC_TestDataFactory.createAccountSalesOrg(defaultShipToAccount, salesOrg);
            INSERT accountSalesOrg;
        }

        List<ccrz__E_Product__c> products = createChildProducts();

        System.runAs(testUser)
        {
            cart = createCart();
            createCartItems(cart, products);

            ccrz__E_Cart__c newCart = new ccrz__E_Cart__c();
            List<ccrz__E_CartItem__c> oldCartItems = new List<ccrz__E_CartItem__c>();
            List<ccrz__E_CartItem__c> newCartItems = new List<ccrz__E_CartItem__c>();

            oldCartItems = getCartItems(cart.Id);

            List<ARC_DTO_Order.OrderLine> orderLines = new List<ARC_DTO_Order.OrderLine>();
            Integer i = 0;
            Decimal itemVatTotal = 0.0;
            for (ccrz__E_CartItem__c cartItem : cartItems) {
                ARC_DTO_Order.OrderLine orderLine = new ARC_DTO_Order.OrderLine();
                i++;
                orderLine.eanOrUpc = String.valueof(i);
                orderLine.lineNumber = i;
                orderLine.quantity = Integer.valueOf(cartItem.ccrz__Quantity__c);
                orderLine.lineNetTotal = Decimal.valueof(i) * 10.00;
                orderLine.deliveryDate = cartItem.ccrz__RequestDate__c;
                orderLine.internalId = ARC_DTO_Order.generateOrderLineInternalId(cart.id, 1, i, cartItem.ccrz__RequestDate__c);
                orderLines.add(orderLine);
                itemVatTotal += Decimal.valueof(i) * 2.00;
            }

            ARC_DTO_Order.OrderItem orderItem = new ARC_DTO_Order.OrderItem();
            orderItem.itemNumber = 1;
            orderItem.articleNumber = colorProduct.ccrz__SKU__c ;
            orderItem.itemVatTotal = itemVatTotal;
            orderItem.lines = orderLines;
            List<ARC_DTO_Order.Orderitem> orderItems = new List<ARC_DTO_Order.Orderitem>();
            orderItems.add(orderItem);

            ARC_DTO_Order.Order order = new ARC_DTO_Order.Order();
            order.items = orderItems;
            order.internalId = cart.id;
            order.externalUniqueId = '99999999-O-9999999999';
            order.storefront = cart.ccrz__Storefront__c;
            order.salesOrg = salesOrg.EUID__c;
            order.soldTo = defaultShipToAccount.EUID__c;
            order.shipTo = order.soldTo;

            ARC_DTO_Order.Order paramOrder = order;

            ARC_DTO_Order.OrderLine orderLine2 = new ARC_DTO_Order.OrderLine();
            orderLine2.eanOrUpc = '2';
            orderLine2.internalId = String.valueOf(cartItems.size()+1);
            orderLine2.quantity = 10;
            orderLine2.lineNetTotal = 100;
            order.items[0].lines.add(orderLine2);

            ARC_DTO_Order.OrderLine orderLine3 = new ARC_DTO_Order.OrderLine();
            orderLine3.eanOrUpc = '3';
            orderLine3.internalId = String.valueOf(cartItems.size()+2);
            orderLine3.quantity = 15;
            orderLine3.lineNetTotal = 100;
            order.items[0].lines.add(orderLine3);

            order.items[0].lines.remove(0);

            for( ARC_DTO_Order.OrderLine orderLine : order.items[0].lines){
                orderLine.lineNetTotal = 49;
            }
            Set<String> productIdentifiers = new Set<String>();
            for (ARC_DTO_Order.OrderItem orderItemDto : order.items) {
                for (ARC_DTO_Order.OrderLine orderLine : orderItemDto.lines) {
                    if (orderLine.eanOrUpc != null) {
                        productIdentifiers.add(orderLine.eanOrUpc);

                        productIdentifiers.add(ARC_Utilities.leftPadWithZeros(orderLine.eanOrUpc, EAN_CODE_LENGTH));
                        productIdentifiers.add(ARC_Utilities.leftPadWithZeros(orderLine.eanOrUpc, UPC_CODE_LENGTH));
                    }
                }
            }
            mocks.startStubbing();
                fflib_Match.matches(new fflib_MatcherDefinitions.AnyObject());
                mocks.when(orderService.calculatePrice(paramOrder)).thenReturn(order);
                mocks.when(testProductService.getProductsForEANorUPC(productIdentifiers)).thenReturn(products);
            mocks.stopStubbing();

            Test.startTest();

            newCart = cartService.calculateNetPrices(cart);

            Test.stopTest();

            newCartItems = getCartItems(cart.Id);

            System.debug(oldCartItems);
            System.debug(newCartItems);
            for (ccrz__E_CartItem__c newCartItem : newCartItems) {
                System.assertNotEquals(oldCartItems[0], newCartItem, 'product did not get deleted');
                System.assertEquals(49,newCartItem.ccrz__SubAmount__C,'Subamount did not get overridden');
            }
            System.assertEquals(oldCartItems.Size(),newCartItems.size());
        }
    }


    private static void createCartItems(ccrz__E_Cart__c createdCart, List<ccrz__E_Product__c> products) {
       cartItems = ARC_TestDataFactory.createCartItemList(createdCart, products);
        Integer i = 0;
        for(ccrz__E_CartItem__c cartItem : cartItems) {
            cartItem.ccrz__CartItemId__c = 'id' + i ;
            i++;
        }
        INSERT cartItems;
    }


    private static List<ccrz__E_Product__c> createChildProducts()
    {
        List<ccrz__E_Product__c> sizeProducts = new List<ccrz__E_Product__c>();

        System.runAs(testPortalAccountOwner)
        {
            colorProduct = ARC_TestDataFactory.createProducts(1).get(0);
            insert colorProduct;

            sizeProducts = ARC_TestDataFactory.createProducts(2);
            sizeProducts = ARC_TestDataFactory.createChildProducts(colorProduct, sizeProducts);
            Integer ean =1;
            for (ccrz__E_Product__c product : sizeProducts) {
                product.EAN__C = String.valueOf(ean);
                ean++;
            }
            insert sizeProducts;

            List<ccrz__E_CompositeProduct__c> compositeProducts = ARC_TestDataFactory.createCompositeProducts(
                colorProduct,
                sizeProducts
            );
            insert compositeProducts;
        }

        return sizeProducts;
    }

    private static ccrz__E_Cart__c createCart() {
        cart = ARC_TestDataFactory.createCart('TestCart', contact, defaultShipToAccount);
        cart.ccrz__Storefront__c = 'adidasReorder';
        INSERT cart;

        return cart;
    }


    private static List<ccrz__E_CartItem__c> getCartItems (Id cartId)
    {
        return [
            SELECT
                Id,
                ccrz__Price__c,
                ccrz__Quantity__c,
                ccrz__SubAmount__c,
                ccrz__CartItemId__c,
                ccrz__Product__r.ccrz__ParentProduct__c,
                ccrz__Product__r.ccrz__ProductType__c
            FROM
                ccrz__E_CartItem__c
            WHERE
                ccrz__Cart__c = :cartId
        ];
    }

    @isTest
    public static void testQueryCartWithItems()
    {
        List<ccrz__E_Product__c> productList;
        List<ccrz__E_Product__c> otherProductList;
        ccrz__E_Cart__c otherCart;
        Set<Id> productIdSet = new Set<Id>();

        setUp();

        System.runAs(testPortalAccountOwner)
        {
            productList = ARC_TestDataFactory.createProducts(3);
            insert productList;
            for (ccrz__E_Product__c product : productList) {
                productIdSet.add(product.Id);
            }

            insert ARC_TestDataFactory.createCartItemList(testCart, productList);

            otherProductList = ARC_TestDataFactory.createProducts(10);
            insert otherProductList;

            otherCart = new ccrz__E_Cart__c(
                ccrz__Name__c = 'TestCart2',
                ShipTo__c = defaultShipToAccount.Id,
                ccrz__EncryptedId__c = encryptedCartId + '_other'
            );
            insert otherCart;

            insert ARC_TestDataFactory.createCartItemList(otherCart, otherProductList);
        }

        System.runAs(testUser)
        {
            Test.startTest();
            cart = cartService.queryCartWithItems(encryptedCartId);
            Test.stopTest();
        }

        System.assertNotEquals(null, cart, 'Cart was not found!');
        System.assertEquals(encryptedCartId, cart.ccrz__EncryptedId__c, 'Wrong cart was queried!');
        System.assertEquals(productIdSet.size(), cart.ccrz__E_CartItems__r.size(), 'Wrong number of cart items!');
        for (ccrz__E_CartItem__c cartItem : cart.ccrz__E_CartItems__r) {
            System.assert(productIdSet.contains(cartItem.ccrz__Product__c), 'Unexpected cart content!');
        }
    }

    @isTest
    public static void testQueryActiveCartsByStorefront()
    {
        final String storefrontName = 'TestStorefront';
        List<ccrz__E_Product__c> productList;
        ccrz__E_Cart__c activeCart;
        List<ccrz__E_Cart__c> cartsBefore;
        List<ccrz__E_Cart__c> cartsAfter;

        setUp();

        System.runAs(testPortalAccountOwner)
        {
            productList = ARC_TestDataFactory.createProducts(10);
            insert productList;
        }

        System.runAs(testUser)
        {
            activeCart = new ccrz__E_Cart__c(
                ccrz__Name__c = 'Active Cart',
                ccrz__ActiveCart__c = false,
                ccrz__Storefront__c = storefrontName,
                ccrz__User__c = testUser.Id,
                ShipTo__c = defaultShipToAccount.Id,
                ccrz__EncryptedId__c = encryptedCartId + '_active'
            );
            insert activeCart;

            insert ARC_TestDataFactory.createCartItemList(activeCart, productList);

            Test.startTest();
            cartsBefore = cartService.queryActiveCartsByStorefront('TestStorefront');

            activeCart.ccrz__ActiveCart__c = true;
            update activeCart;

            cartsAfter = cartService.queryActiveCartsByStorefront('TestStorefront');
            Test.stopTest();
        }

        System.assertNotEquals(null, cartsBefore, 'Invalid return value: null!');
        System.assertNotEquals(null, cartsAfter, 'Invalid return value: null!');
        System.assertEquals(0, cartsBefore.size(), 'Invalid active cart was found!');
        System.assertEquals(1, cartsAfter.size(), 'Active cart was not found!');
        System.assertEquals(activeCart.Id, cartsAfter.get(0).Id, 'Invalid active cart was found!');
    }

    @isTest
    public static void getSoldTo()
    {
        setUp();
        setUpMocks();

        mocks.startStubbing();
            mocks.when(testAccountService.getSoldTo(shipToAccount.Id)).thenReturn(shipToAccount);
        mocks.stopStubbing();

        Account soldToAccount = new Account();
        Test.startTest();
            soldToAccount = cartService.getSoldTo(cart);
        Test.stopTest();

        System.assertEquals(soldToAccount,shipToAccount,'SoldTo not found');
    }

    @isTest
    public static void removeProductsFromCartTest() {

        setUp();

        List<Id> productIds = new List<Id>();

        System.runAs(testPortalAccountOwner) {
            List<ccrz__E_Product__c> products = ARC_TestDataFactory.createProducts(3);
            products.get(1).ccrz__ProductType__c = ARC_Constants.PRODUCT_TYPE_EAN;
            insert products;
            productIds.add(products.get(1).Id);

            insert ARC_TestDataFactory.createCartItemList(testCart, products);

            Boolean succes = false;

            Test.startTest();
            succes = cartService.removeProductsFromCart(testUser.Id, testCart.ccrz__EncryptedId__c, productIds);
            Test.stopTest();

            List<ccrz__E_Cart__c> cartList = [
                SELECT
                    Id,
                    Name,
                    ccrz__Name__c,
                    ccrz__Storefront__c,
                    ccrz__ActiveCart__c,
                    ccrz__User__c,
                    ccrz__EncryptedId__c, (
                    SELECT
                        Id,
                        ccrz__Product__c,
                        ccrz__Product__r.ccrz__ParentProduct__c,
                        ccrz__ProductType__c,
                        ccrz__Quantity__c,
                        ccrz__RequestDate__c
                    FROM
                        ccrz__E_CartItems__r
                    )
                FROM
                    ccrz__E_Cart__c
                WHERE
                    ccrz__EncryptedId__c = :encryptedCartId
                ORDER BY
                    Id DESC
                LIMIT
                    1
            ];

            System.assertEquals(true, succes, 'Removing failed, succes is false');
            System.assertEquals(2, cartList[0].ccrz__E_CartItems__r.Size(), 'Removing failed, size not equal');
        }
    }

    @isTest
    public static void getCartItemsByCartIdAndGroupIdTest() {

        setUp();

        System.runAs(testPortalAccountOwner)
        {
            List<ccrz__E_Product__c> testListOfProducts = createChildProducts();
            createCartItems(testCart, testListOfProducts);

            ccrz__E_Product__c testPackage = ARC_TestDataFactory.createPackages(1)[0];
            insert testPackage;

            for (ccrz__E_CartItem__c testCartItem : cartItems) {
                testCartItem.GroupId__c = testPackage.Id;
            }
            update cartItems;

            Test.startTest();
            List<ccrz__E_CartItem__c> cartItemsResult = cartService.getCartItemsByCartIdAndGroupId(testCart.ccrz__EncryptedId__c, testPackage.Id);
            Test.stopTest();

            System.assertEquals(2, cartItemsResult.size());
            for(ccrz__E_CartItem__c cartItemResult : cartItemsResult) {
                for (ccrz__E_CartItem__c testCartItem : cartItems) {
                    if(cartItemResult.ccrz__Product__r.ccrz__ProductType__c == testCartItem.ccrz__Product__r.ccrz__ProductType__c)
                        System.assertEquals(cartItemResult.Id, testCartItem.Id);
                }
            }
        }
    }

    @isTest
    public static void getCartItemsByParentProductIdsTest() {

        setUp();

        System.runAs(testPortalAccountOwner)
        {
            List<ccrz__E_Product__c> testListOfProducts = createChildProducts();
            update testListOfProducts;
            createCartItems(testCart, testListOfProducts);

            cartItems = getCartItems(testCart.Id);

            Set<Id> singleAggregatedProductsTest = new Set<Id>();

            for (ccrz__E_CartItem__c testCartItem : cartItems) {
                singleAggregatedProductsTest.add(testCartItem.ccrz__Product__r.ccrz__ParentProduct__c);
            }
            update cartItems;

            Test.startTest();
            List<ccrz__E_CartItem__c> cartItemsResult = cartService.getCartItemsByParentProductIds(testCart.Id, singleAggregatedProductsTest);
            Test.stopTest();

            System.assertEquals(2, cartItemsResult.size());
        }
    }


    @IsTest
    private static void testGetActive ()
    {
        setUp();
        setUpMocks();

        String storefront = 'storefront';

        mocks.startStubbing();
        mocks.when(cartDataGateway.getActive(testUser.Id, storefront)).thenReturn(testCart);
        mocks.stopStubbing();

        System.runAs(testUser) {

            Test.startTest();

            ccrz__E_Cart__c activeCart = cartService.getActiveCart(testUser, storefront);

            Test.stopTest();

            System.assertEquals(testCart.Id, activeCart.Id);
        }
    }


    @IsTest
    public static void testGetWishlists()
    {
        setUp();
        setUpMocks();

        expectCartGatewayToReturnWishlist('adidasReorder', new List<ccrz__E_Cart__c>{ cart });

        List<ccrz__E_Cart__c> carts;

        System.runAs(testUser) {
            Test.startTest();
            carts = cartService.getWishlists('adidasReorder');
            Test.stopTest();
        }

        System.assertEquals(1, carts.size());
        System.assertEquals(cart.ccrz__EncryptedId__c, carts.get(0).ccrz__EncryptedId__c);
    }

    private static void expectCartGatewayToReturnWishlist(String storefront, List<ccrz__E_Cart__c> carts)
    {
        mocks.startStubbing();
        mocks.when(cartDataGateway.getWishlistsWithItems(storefront)).thenReturn(carts);
        mocks.stopStubbing();
    }


    @IsTest
    public static void testCreateCarts()
    {
        setUp();

        ccrz.cc_CallContext.storefront = 'adidasReorder';
        ccrz.cc_CallContext.userCurrency = 'EUR';
        ccrz.cc_CallContext.storeFrontSettings = new Map<String, Object>{'DefaultLocale__c' => 'en_GB'};

        List<Map<String, Object>> carts = new List<Map<String, Object>>{
            new Map<String, Object>{
                'activeCart' => true,
                'cartType' => 'Cart',
                'name' => 'nextCart',
                'storefront' => 'adidasReorder'
            }
        };

        List<ccrz__E_Cart__c> createdCarts;
        System.runAs(testUser) {
            ccrz.cc_CallContext.storefront = 'adidasReorder';
            ccrz.cc_CallContext.userCurrency = 'EUR';
            ccrz.cc_CallContext.storeFrontSettings = new Map<String, Object>{'DefaultLocale__c' => 'en_GB'};

            Test.startTest();
            createdCarts = cartService.createCarts(carts);
            Test.stopTest();
        }

        System.debug(createdCarts);
        System.assertEquals(1, createdCarts.size());
        ccrz__E_Cart__c refetchedCart = [
            SELECT
                ccrz__Name__c
            FROM
                ccrz__E_Cart__c
            WHERE
                ccrz__EncryptedId__c = :createdCarts.get(0).ccrz__EncryptedId__c
        ];

        System.assertEquals('nextCart', refetchedCart.ccrz__Name__c);
    }

    @isTest
    public static void cloneCartTest() {

        List<ccrz__E_Product__c> parentProducts = new List<ccrz__E_Product__c>();
        List<ccrz__E_Product__c> childProducts = new List<ccrz__E_Product__c>();
        map<ccrz__E_Product__c, List<ccrz__E_Product__c>> parentProductWithProductsChilds = new map<ccrz__E_Product__c, List<ccrz__E_Product__c>>();

        setUp();

        System.runAs(testPortalAccountOwner) {

            parentProducts = ARC_TestDataFactory.createProducts(1);
            insert parentProducts;

            childProducts = ARC_TestDataFactory.createProducts(2);
            insert childProducts;

            parentProductWithProductsChilds.put(parentProducts[0], childProducts);

            List<ccrz__E_CartItem__c> testCartItems = ARC_TestDataFactory.createCartItems(testCart, parentProductWithProductsChilds);

            Test.startTest();
            ccrz__E_Cart__c cartCloned = cartService.cloneCartWithItems(testCart.Id);
            Test.stopTest();

            System.assert(cartCloned != null);

        }
    }


    @IsTest
    private static void testGetShifted()
    {
        setUp();
        setUpMocks();

        mocks.startStubbing();
        mocks.when(cartConfiguration.getMajorCartItemType()).thenReturn('Major');
        mocks.when(cartConfiguration.getMinorCartItemType()).thenReturn('Minor');
        mocks.stopStubbing();


        String cartId = 'CART_ID';
        String sku = '288022';
        String size = '540';

        List<ccrz__E_CartItem__c> pastCartItems = new List<ccrz__E_CartItem__c>();
        Set<String> shiftedItemIds = new Set<String>();

        ccrz__E_CartItem__c cartItem1 = new ccrz__E_CartItem__c(
            ccrz__Cart__r = new ccrz__E_Cart__c(ccrz__CartId__c = cartId, ccrz__EncryptedId__c = cartId),
            ccrz__CartItemId__c = cartId + '-' + sku + '-' + size + '-' + ((DateTime)Date.today().addDays(-3)).format('yyyy-MM-dd'),
            ccrz__cartItemType__c = cartConfiguration.getMinorCartItemType(),
            ccrz__Price__c = 0.0,
            ccrz__Product__r = new ccrz__E_Product__c(EUID__c = sku + '-' + size, ccrz__SKU__c = sku + '-' + size),
            ccrz__Quantity__c = 10.0,
            ccrz__RequestDate__c = Date.today().addDays(-3),
            ccrz__SubAmount__c = 0.0
        );
        shiftedItemIds.add(cartItem1.ccrz__CartItemId__c);
        pastCartItems.add(cartItem1);

        ccrz__E_CartItem__c cartItem2 = new ccrz__E_CartItem__c(
            ccrz__Cart__r = new ccrz__E_Cart__c(ccrz__CartId__c = cartId, ccrz__EncryptedId__c = cartId),
            ccrz__CartItemId__c = cartId + '-' + sku + '-' + size + '-' + ((DateTime)Date.today().addDays(-5)).format('yyyy-MM-dd'),
            ccrz__cartItemType__c = cartConfiguration.getMinorCartItemType(),
            ccrz__Price__c = 0.0,
            ccrz__Product__r = new ccrz__E_Product__c(EUID__c = sku + '-' + size, ccrz__SKU__c = sku + '-' + size),
            ccrz__Quantity__c = 10.0,
            ccrz__RequestDate__c = Date.today().addDays(-5),
            ccrz__SubAmount__c = 0.0
        );
        shiftedItemIds.add(cartItem2.ccrz__CartItemId__c);
        pastCartItems.add(cartItem2);

        ccrz__E_CartItem__c cartItem3 = new ccrz__E_CartItem__c(
            ccrz__Cart__r = new ccrz__E_Cart__c(ccrz__CartId__c = cartId, ccrz__EncryptedId__c = cartId),
            ccrz__CartItemId__c = cartId + '-' + sku + '-' + size + '-' + ((DateTime)Date.today().addDays(5)).format('yyyy-MM-dd'),
            ccrz__cartItemType__c = cartConfiguration.getMinorCartItemType(),
            ccrz__Price__c = 0.0,
            ccrz__Product__r = new ccrz__E_Product__c(EUID__c = sku + '-' + size, ccrz__SKU__c = sku + '-' + size),
            ccrz__Quantity__c = 15.0,
            ccrz__RequestDate__c = Date.today().addDays(5),
            ccrz__SubAmount__c = 0.0
        );
        pastCartItems.add(cartItem3);

        ccrz__E_CartItem__c cartItem4 = new ccrz__E_CartItem__c(
            ccrz__Cart__r = new ccrz__E_Cart__c(ccrz__CartId__c = cartId, ccrz__EncryptedId__c = cartId),
            ccrz__CartItemId__c = cartId + '-' + '288022',
            ccrz__cartItemType__c = cartConfiguration.getMajorCartItemType(),
            ccrz__Product__r = new ccrz__E_Product__c(EUID__c = sku, ccrz__SKU__c = sku),
            ccrz__Quantity__c = 1.0
        );
        pastCartItems.add(cartItem4);


        Test.startTest();
        List<ccrz__E_CartItem__c> shiftedItems = cartService.getShifted(pastCartItems);
        Test.stopTest();

        for (ccrz__E_CartItem__c ci : shiftedItems) {
            if (shiftedItemIds.contains(ci.ccrz__CartItemId__c)) {
                System.assertEquals('REMOVE_AFTER_SHIFT', ci.ccrz__ItemStatus__c);
            } else {
                System.assertNotEquals('REMOVE_AFTER_SHIFT', ci.ccrz__ItemStatus__c);
            }

            if (ci.ccrz__RequestDate__c == Date.today()) {
                System.assertEquals(20.0, ci.ccrz__Quantity__c);
                System.assertEquals('288022-540', ci.ccrz__Product__r.EUID__c);
            }

            if (ci.ccrz__RequestDate__c > Date.today()) {
                System.assertEquals(cartItem3.ccrz__CartItemId__c, ci.ccrz__CartItemId__c);
                System.assertEquals(15, ci.ccrz__Quantity__c);
            }

            if (ci.ccrz__CartItemId__c == cartItem4.ccrz__CartItemId__c) {
                System.assertEquals(cartItem4, ci);
            }
        }
    }

    @isTest
    public static void testRemoveAllCartItems() {
        List<ccrz__E_Product__c> productList;
        Set<Id> productIdSet = new Set<Id>();
        Boolean success;

        String defaultCartName = 'Default Cartname';

        setUp();
        setUpMocks();

        System.runAs(testPortalAccountOwner) {
            productList = ARC_TestDataFactory.createProducts(3);
            INSERT productList;

            for (ccrz__E_Product__c product : productList) {
                productIdSet.add(product.Id);
            }
            INSERT ARC_TestDataFactory.createCartItemList(testCart, productList);

            testCart.ccrz__Name__c = 'TestCart';
            UPDATE testCart;
        }

        testCart = [
            SELECT
                Id,
                ccrz__Name__c,
                (
                    SELECT
                        Name
                    FROM
                        ccrz__E_CartItems__r
                )
            FROM
                ccrz__E_Cart__c
            WHERE
                Id = :testCart.Id
            ORDER BY
                Id DESC
            LIMIT
                1
        ];

        System.runAs(testUser) {
            Test.startTest();
            success = cartService.removeAllCartItems(testCart);
            Test.stopTest();
        }

        List<ccrz__E_CartItem__c> cartItems = getCartItems(testCart.Id);

        System.assert(success);
        System.assert(cartItems.isEmpty());
    }

    @isTest
    public static void testSetDefaultCartName() {
        List<ccrz__E_Product__c> productList;
        Set<Id> productIdSet = new Set<Id>();
        Boolean success;

        String defaultCartName = 'Personal reference';

        setUp();
        setUpMocks();

        System.runAs(testPortalAccountOwner) {
            productList = ARC_TestDataFactory.createProducts(3);
            INSERT productList;

            for (ccrz__E_Product__c product : productList) {
                productIdSet.add(product.Id);
            }
            INSERT ARC_TestDataFactory.createCartItemList(testCart, productList);

            testCart.ccrz__Name__c = 'TestCart';
            UPDATE testCart;
        }

        testCart = [
            SELECT
                Id,
                ccrz__Name__c,
                ccrz__Storefront__c,
                (
                    SELECT
                        Id
                    FROM
                        ccrz__E_CartItems__r
                )
            FROM
                ccrz__E_Cart__c
            WHERE
                Id = :testCart.Id
            ORDER BY
                Id DESC
            LIMIT
                1
        ];

        mocks.startStubbing();
        mocks.when(testI18nL18nService.getPageLabel(testUser.LocaleSidKey, testCart.ccrz__Storefront__c, 'Cart', 'ARC_your_personal_refnr')).thenReturn(defaultCartName);
        mocks.when(testUserService.getCurrentUser()).thenReturn(testUser);
        mocks.stopStubbing();

        System.runAs(testUser) {
            Test.startTest();
            success = cartService.setDefaultCartName(testCart);
            Test.stopTest();
        }

        testCart = [
            SELECT
                ccrz__Name__c
            FROM
                ccrz__E_Cart__c
            WHERE
                Id = :testCart.Id
            ORDER BY
                Id DESC
            LIMIT
                1
        ];

        System.assertEquals('Personal reference', testCart.ccrz__Name__c);
    }

    @isTest
    public static void testSetDefaultCartNameFailure() {
        List<ccrz__E_Product__c> productList;
        Set<Id> productIdSet = new Set<Id>();
        ccrz__E_Cart__c newCart = new ccrz__E_Cart__c(
            ccrz__Name__c = 'TestCart'
        );
        Boolean success;

        String defaultCartName = 'Personal Reference';

        setUp();
        setUpMocks();

        System.runAs(testPortalAccountOwner) {
            productList = ARC_TestDataFactory.createProducts(3);
            INSERT productList;

            for (ccrz__E_Product__c product : productList) {
                productIdSet.add(product.Id);
            }
            INSERT ARC_TestDataFactory.createCartItemList(testCart, productList);
        }

        mocks.startStubbing();
        mocks.when(testI18nL18nService.getPageLabel(testUser.LocaleSidKey, cart.ccrz__Storefront__c, 'Cart', 'ARC_your_personal_refnr')).thenReturn(defaultCartName);
        mocks.when(testUserService.getCurrentUser()).thenReturn(testUser);
        mocks.stopStubbing();

        System.runAs(testUser) {
            Test.startTest();
            success = cartService.setDefaultCartName(newCart);
            Test.stopTest();
        }

        System.assert(!success);
    }
}