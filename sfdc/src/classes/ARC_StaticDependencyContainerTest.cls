/**
 * Testing the static dependency registry - needs strict assertions to identifie the SAME instance
 */
@isTest
public with sharing class ARC_StaticDependencyContainerTest
{
    private static fflib_ApexMocks mocks = new fflib_ApexMocks();

    private static ARC_Configuration_Listing listingConfiguration;

    private static ARC_StaticDependencyContainer dependencyContainer;


    private static void setUp ()
    {
        mocks = new fflib_ApexMocks();

        listingConfiguration = new ARC_Mock_Configurations.ARC_Mock_ListingConfiguration(mocks);

        dependencyContainer = new ARC_StaticDependencyContainer();
    }


    private static testMethod  void testGetCatalogueItemServiceReturnsTheSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_Service_CatalogueItem firstCatalogueItemService = dependencyContainer.getCatalogueItemService();
        ARC_Service_CatalogueItem secondCatalogueItemService = dependencyContainer.getCatalogueItemService();
        ARC_Service_CatalogueItem thirdCatalogueItemService = new ARC_Service_CatalogueProducts();

        Test.stopTest();

        System.assertEquals(secondCatalogueItemService, firstCatalogueItemService);
        System.assertNotEquals(thirdCatalogueItemService, firstCatalogueItemService);
    }


    private static testMethod  void testGetCategoryServiceReturnsTheSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_Service_Category firstInstance  = dependencyContainer.getCategoryService();
        ARC_Service_Category secondInstance = dependencyContainer.getCategoryService();
        ARC_Service_Category thirdInstance = new ARC_Service_CcrzCategory(
            dependencyContainer.getCategoryDataGateway(),
            dependencyContainer.getUserService(),
            dependencyContainer.getSalesOrgService()
        );

        Test.stopTest();

        System.assertEquals(secondInstance, firstInstance);
        System.assertNotEquals(thirdInstance, firstInstance);
    }


    private static void testGetCategoryMediaServiceReturnsTheSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_Service_CategoryMedia firstCategoryMediaService = dependencyContainer.getCategoryMediaService();
        ARC_Service_CategoryMedia secondCategoryMediaService = dependencyContainer.getCategoryMediaService();
        ARC_Service_CategoryMedia thirdCategoryMediaService = new ARC_Service_CcrzCategoryMedia();

        Test.stopTest();

        System.assertEquals(secondCategoryMediaService, firstCategoryMediaService);
        System.assertNotEquals(thirdCategoryMediaService, firstCategoryMediaService);
    }

    @IsTest
    private static void testGetConfigurationReturnsTheSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_Configuration firstConfiguration = dependencyContainer.getConfiguration();
        ARC_Configuration secondConfiguration = dependencyContainer.getConfiguration();
        ARC_Configuration thirdConfiguraton = new ARC_LocalConfiguration();

        Test.stopTest();

        System.assertEquals(secondConfiguration, firstConfiguration);
        System.assertNotEquals(thirdConfiguraton, firstConfiguration);
    }


    @IsTest
    private static void testGetColorServiceReturnsTheSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_Service_Colors firstColorService = dependencyContainer.getColorService();
        ARC_Service_Colors secondColorService = dependencyContainer.getColorService();
        ARC_Service_Colors thirdColorService = new ARC_Service_Colors();

        Test.stopTest();

        System.assertEquals(secondColorService, firstColorService);
        System.assertNotEquals(thirdColorService, firstColorService);
    }


    @IsTest
    private static void testGetDeliveryBlockService ()
    {
        setUp();

        Test.startTest();

        ARC_Service_DeliveryBlock firstDeliveryBlockService = dependencyContainer.getDeliveryBlockService();
        ARC_Service_DeliveryBlock secondDeliveryBlockService = dependencyContainer.getDeliveryBlockService();
        ARC_Service_DeliveryBlock thirdDeliveryBlockService = new ARC_Service_SalesOrgDeliveryBlock();

        Test.stopTest();

        System.assertEquals(secondDeliveryBlockService, firstDeliveryBlockService);
        System.assertNotEquals(thirdDeliveryBlockService, firstDeliveryBlockService);
    }


    @IsTest
    private static void testGetErrorServiceReturnsTheSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_Service_Error firstErrorService = dependencyContainer.getErrorService();
        ARC_Service_Error secondErrorService = dependencyContainer.getErrorService();
        ARC_Service_Error thirdErrorService = new ARC_Service_ErrorLog();

        Test.stopTest();

        System.assertEquals(secondErrorService, firstErrorService);
        System.assertNotEquals(thirdErrorService, firstErrorService);
    }


    @IsTest
    private static void testGetListingServiceReturnsTheSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_Service_Listing firstListingService = dependencyContainer.getListingService();
        ARC_Service_Listing secondListingService = dependencyContainer.getListingService();
        ARC_Service_Listing thirdListingService = new ARC_Service_LeTableListing(listingConfiguration);

        Test.stopTest();

        System.assertEquals(secondListingService, firstListingService);
        System.assertNotEquals(thirdListingService, firstListingService);
    }


    @IsTest
    private static void testGetProductCategoryServiceReturnsTheSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_Service_ProductCategory firstProductCategoryService = dependencyContainer.getProductCategoryService();
        ARC_Service_ProductCategory secondProductCategoryService = dependencyContainer.getProductCategoryService();
        ARC_Service_ProductCategory thirdProductCategoryService = new ARC_Service_ProductGroup();

        Test.stopTest();

        System.assertEquals(secondProductCategoryService, firstProductCategoryService);
        System.assertNotEquals(thirdProductCategoryService, firstProductCategoryService);
    }


    @IsTest
    private static void testGetProductListServiceReturnsTheSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_Service_ProductList firstProductListService = dependencyContainer.getProductListService();
        ARC_Service_ProductList secondProductListService = dependencyContainer.getProductListService();
        ARC_Service_ProductList thirdProductListService = new ARC_Service_ProductList();

        Test.stopTest();

        System.assertEquals(secondProductListService, firstProductListService);
        System.assertNotEquals(thirdProductListService, firstProductListService);
    }


    @IsTest
    private static void testGetProductServiceReturnsTheSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_Service_Product firstProductService = dependencyContainer.getProductService();
        ARC_Service_Product secondProductService = dependencyContainer.getProductService();
        ARC_Service_Product thirdProductService = new ARC_Service_CcrzProduct(
            dependencyContainer.getConfiguration().getProductConfiguration(),
            dependencyContainer.getResourceService(),
            dependencyContainer.getProductFilterService(),
            dependencyContainer.getSalesOrgService(),
            dependencyContainer.getStorefrontService(),
            dependencyContainer.getGlobalCacheService(),
            dependencyContainer.getProductDataGateway()
        );

        Test.stopTest();

        System.assertEquals(secondProductService, firstProductService);
        System.assertNotEquals(thirdProductService, firstProductService);
    }


    @IsTest
    private static void testGetProductFilterServiceReturnsTheSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_Service_ProductFilter firstProductFilterService = dependencyContainer.getProductFilterService();
        ARC_Service_ProductFilter secondProductFilterService = dependencyContainer.getProductFilterService();
        ARC_Service_ProductFilter thirdProductFilterService = new ARC_Service_CcrzProductFilter();

        Test.stopTest();

        System.assertEquals(secondProductFilterService, firstProductFilterService);
        System.assertNotEquals(thirdProductFilterService, firstProductFilterService);
    }


    @IsTest
    private static void testGetAccountServiceReturnsTheSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_Service_Account firstAccountService = dependencyContainer.getAccountService();
        ARC_Service_Account secondAccountService = dependencyContainer.getAccountService();
        ARC_Service_Account thirdAccountService = new ARC_Service_sfdcAccount(
            dependencyContainer.getAccountDataGateway(),
            dependencyContainer.getUserDataGateway(),
            dependencyContainer.getSalesOrgService(),
            dependencyContainer.getOrderBlockService()
        );

        Test.stopTest();

        System.assertEquals(secondAccountService, firstAccountService);
        System.assertNotEquals(thirdAccountService, firstAccountService);
    }


    @IsTest
    private static void testGeti18nL18nServiceReturnsTheSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_Service_i18nL18n firsti18nL18nService = dependencyContainer.geti18nL18nService();
        ARC_Service_i18nL18n secondi18nL18nService = dependencyContainer.geti18nL18nService();
        ARC_Service_i18nL18n thirdi18nL18nService = new ARC_Service_Globali18nL18n();

        Test.stopTest();

        System.assertEquals(secondi18nL18nService, firsti18nL18nService);
        System.assertNotEquals(thirdi18nL18nService, firsti18nL18nService);
    }


    @IsTest
    private static void testGetMessageServiceReturnsTheSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_Service_Message firstMessageService = dependencyContainer.getMessageService();
        ARC_Service_Message secondMessageService = dependencyContainer.getMessageService();
        ARC_Service_Message thirdMessageService = new ARC_Service_GlobalMessage();

        Test.stopTest();

        System.assertEquals(secondMessageService, firstMessageService);
        System.assertNotEquals(thirdMessageService, firstMessageService);
    }


    @IsTest
    private static void testGetSeasonServiceReturnsTheSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_Service_Season firstSeasonService = dependencyContainer.getSeasonService();
        ARC_Service_Season secondSeasonService = dependencyContainer.getSeasonService();
        ARC_Service_Season thirdSeasonService = new ARC_Service_LocalSeason(
            dependencyContainer.getSeasonDataGateway(),
            dependencyContainer.getConfiguration().getStoreConfiguration(),
            dependencyContainer.getAccountService(),
            dependencyContainer.getSalesOrgService()
        );

        Test.stopTest();

        System.assertEquals(secondSeasonService, firstSeasonService);
        System.assertNotEquals(thirdSeasonService, firstSeasonService);
    }


    @IsTest
    private static void testGetSecurityServiceReturnsTheSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_Service_Security firstSecurityService = dependencyContainer.getSecurityService();
        ARC_Service_Security secondSecurityService = dependencyContainer.getSecurityService();
        ARC_Service_Security thirdSecurityService = new ARC_Service_PortalSecurity();

        Test.stopTest();

        System.assertEquals(secondSecurityService, firstSecurityService);
        System.assertNotEquals(thirdSecurityService, firstSecurityService);
    }


    @IsTest
    private static void testGetSpecServiceReturnsTheSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_Service_Spec firstInstance = dependencyContainer.getSpecService();
        ARC_Service_Spec secondInstance = dependencyContainer.getSpecService();
        ARC_Service_Spec thirdInstance = new ARC_Service_CcrzSpec();

        Test.stopTest();

        System.assertEquals(secondInstance, firstInstance);
        System.assertNotEquals(thirdInstance, firstInstance);
    }


    @IsTest
    private static void testGetStorefrontServiceReturnsTheSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_Service_Storefront firstStorefrontService = dependencyContainer.getStorefrontService();
        ARC_Service_Storefront secondStorefrontService = dependencyContainer.getStorefrontService();
        ARC_Service_Storefront thirdStorefrontService = new ARC_Service_CcrzStorefront();

        Test.stopTest();

        System.assertEquals(secondStorefrontService, firstStorefrontService);
        System.assertNotEquals(thirdStorefrontService, firstStorefrontService);
    }


    @IsTest
    private static void testGetProductAvailabilityService ()
    {
        setUp();

        Test.startTest();

        ARC_Service_ProductAvailability firstProductAvailabilityService = dependencyContainer.getProductAvailabilityService();
        ARC_Service_ProductAvailability secondProductAvailabilityService = dependencyContainer.getProductAvailabilityService();
        ARC_Service_ProductAvailability thirdProductAvailabilityService = new ARC_Service_ProductAvailabilityDate();

        Test.stopTest();

        System.assertEquals(secondProductAvailabilityService, firstProductAvailabilityService);
        System.assertNotEquals(thirdProductAvailabilityService, firstProductAvailabilityService);
    }


    @IsTest
    private static void testGetProdRecEngineReturnsTheSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_Service_ProductRecommendationEngine firstProdRecService = dependencyContainer.getProductRecommendationEngine();
        ARC_Service_ProductRecommendationEngine secondProdRecService = dependencyContainer.getProductRecommendationEngine();
        ARC_Service_ProductRecommendationEngine thirdProdRecService = new ARC_Service_SimilarProdRecEngine();

        Test.stopTest();

        System.assertEquals(firstProdRecService, secondProdRecService);
        System.assertNotEquals(firstProdRecService, thirdProdRecService);
    }


    @IsTest
    private static void testGetProductStatusServiceReturnsSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_Service_ProductStatus firstProdRecService = dependencyContainer.getProductStatusService();
        ARC_Service_ProductStatus secondProdRecService = dependencyContainer.getProductStatusService();
        ARC_Service_ProductStatus thirdProdRecService = new ARC_Service_SoldOutStatus(
            dependencyContainer.getProductStatusDataGateway()
        );

        Test.stopTest();

        System.assertEquals(firstProdRecService, secondProdRecService);
        System.assertNotEquals(firstProdRecService, thirdProdRecService);
    }


    @IsTest
    private static void testGetSalesOrgServiceReturnsTheSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_Service_SalesOrg firstSalesOrgService = dependencyContainer.getSalesOrgService();
        ARC_Service_SalesOrg secondSalesOrgService = dependencyContainer.getSalesOrgService();
        ARC_Service_SalesOrg thirdSalesOrgService = new ARC_Service_SalesOrg();

        Test.stopTest();

        System.assertEquals(firstSalesOrgService, secondSalesOrgService);
        System.assertNotEquals(firstSalesOrgService, thirdSalesOrgService);
    }


    @IsTest
    private static void testGetContactServiceSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_Service_Contact firstContactService = dependencyContainer.getContactService();
        ARC_Service_Contact secondContactService = dependencyContainer.getContactService();
        ARC_Service_Contact thirdContactService = new ARC_Service_LocalContact(
            dependencyContainer.getContactDataGateway(),
            dependencyContainer.getConfiguration().getUserConfiguration()
        );

        Test.stopTest();

        System.assertEquals(firstContactService, secondContactService);
        System.assertNotEquals(firstContactService, thirdContactService);
    }


	private static void testGetOrderServiceSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_Service_Order firstOrderService = dependencyContainer.getOrderService();
        ARC_Service_Order secondOrderService = dependencyContainer.getOrderService();
        ARC_Service_Order thirdOrderService = new ARC_Service_CcrzOrder();

        Test.stopTest();

        System.assertEquals(firstOrderService, secondOrderService);
        System.assertNotEquals(firstOrderService, thirdOrderService);
    }


	private static void testGetOrderAPISameInstance() {
        setUp();

        Test.startTest();

        ARC_API_Order firstOrderAPI = dependencyContainer.getOrderAPI();
        ARC_API_Order secondOrderAPI = dependencyContainer.getOrderAPI();
        ARC_API_Order thirdOrderAPI = new ARC_API_OrderFactory().create();

        Test.stopTest();

        System.assertEquals(firstOrderAPI, secondOrderAPI);
        System.assertNotEquals(firstOrderAPI, thirdOrderAPI);
    }


    @IsTest
    private static void testGetCartServiceReturnsTheSameInstance() {
        setUp();

        Test.startTest();

        ARC_Service_Cart firstCartService = dependencyContainer.getCartService();
        ARC_Service_Cart secondCartService = dependencyContainer.getCartService();
        ARC_Service_Cart thirdCartService = new ARC_Service_CcrzCart();

        Test.stopTest();

        System.assertEquals(firstCartService, secondCartService);
        System.assertNotEquals(firstCartService, thirdCartService);
    }

	private static void testGetContentPageSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_Service_ContentPage firstContentPageService = dependencyContainer.getContentPageService();
        ARC_Service_ContentPage secondContentPageService = dependencyContainer.getContentPageService();
        ARC_Service_ContentPage thirdontentPageService = new ARC_Service_LocalContentPage();

        Test.stopTest();

        System.assertEquals(firstContentPageService, secondContentPageService);
        System.assertNotEquals(firstContentPageService, thirdontentPageService);
    }

    @IsTest
    private static void testGetUserServiceSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_Service_User firstInstance = dependencyContainer.getUserService ();
        ARC_Service_User secondInstance  = dependencyContainer.getUserService ();
        ARC_Service_User thirdInstance  = new ARC_Service_PortalUser(
            dependencyContainer.getUserDataGateway(),
            dependencyContainer.getErrorService(),
            dependencyContainer.getCache()
        );

        Test.stopTest();

        System.assertEquals(firstInstance, secondInstance);
        System.assertNotEquals(firstInstance, thirdInstance);
    }

    @IsTest
    private static void testGetCacheSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_Cache firstContentPageService = dependencyContainer.getCache();
        ARC_Cache secondContentPageService = dependencyContainer.getCache();
        ARC_Cache thirdontentPageService = new ARC_SessionCache();

        Test.stopTest();

        System.assertEquals(firstContentPageService, secondContentPageService);
        System.assertNotEquals(firstContentPageService, thirdontentPageService);
    }

    @IsTest
    private static void testGetGlobalCacheSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_Cache firstContentPageService = dependencyContainer.getGlobalCacheService();
        ARC_Cache secondContentPageService = dependencyContainer.getGlobalCacheService();
        ARC_Cache thirdontentPageService = new ARC_Service_OrgCache();

        Test.stopTest();

        System.assertEquals(firstContentPageService, secondContentPageService);
        System.assertNotEquals(firstContentPageService, thirdontentPageService);
    }


    @IsTest
    private static void testGetAccountDataGatewayReturnsSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_TDGW_Account firstDependency = dependencyContainer.getAccountDataGateway();
        ARC_TDGW_Account secondDependency = dependencyContainer.getAccountDataGateway();
        ARC_TDGW_Account thirdDepdendency = new ARC_TDGW_AccountSObject();

        Test.stopTest();

        System.assertEquals(secondDependency, firstDependency);
        System.assertNotEquals(thirdDepdendency, firstDependency);
    }


    @IsTest
    private static void testGetCartDataGatewayReturnsSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_TDGW_Cart firstDependency = dependencyContainer.getCartDataGateway();
        ARC_TDGW_Cart secondDependency = dependencyContainer.getCartDataGateway();
        ARC_TDGW_Cart thirdDepdendency = new ARC_TDGW_CartSObject(
            dependencyContainer.getConfiguration().getCartConfiguration()
        );

        Test.stopTest();

        System.assertEquals(secondDependency, firstDependency);
        System.assertNotEquals(thirdDepdendency, firstDependency);
    }


    @IsTest
    private static void testGetCategoryDataGatewayReturnsSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_TDGW_Category firstDependency = dependencyContainer.getCategoryDataGateway();
        ARC_TDGW_Category secondDependency = dependencyContainer.getCategoryDataGateway();
        ARC_TDGW_Category thirdDepdendency = new ARC_TDGW_CategorySObject();

        Test.stopTest();

        System.assertEquals(secondDependency, firstDependency);
        System.assertNotEquals(thirdDepdendency, firstDependency);
    }


    @IsTest
    private static void testGetContactDataGatewayReturnsSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_TDGW_Contact firstDependency = dependencyContainer.getContactDataGateway();
        ARC_TDGW_Contact secondDependency = dependencyContainer.getContactDataGateway();
        ARC_TDGW_Contact thirdDepdendency = new ARC_TDGW_ContactSObject();

        Test.stopTest();

        System.assertEquals(secondDependency, firstDependency);
        System.assertNotEquals(thirdDepdendency, firstDependency);
    }


    @IsTest
    private static void testGetInventoryDataGatewayReturnsSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_TDGW_Inventory firstDependency = dependencyContainer.getInventoryDataGateway();
        ARC_TDGW_Inventory secondDependency = dependencyContainer.getInventoryDataGateway();
        ARC_TDGW_Inventory thirdDepdendency = new ARC_TDGW_InventorySObject();

        Test.stopTest();

        System.assertEquals(secondDependency, firstDependency);
        System.assertNotEquals(thirdDepdendency, firstDependency);
    }


    @IsTest
    private static void testGetPriceListDataGatewayReturnsSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_TDGW_PriceList firstDependency = dependencyContainer.getPriceListDataGateway();
        ARC_TDGW_PriceList secondDependency = dependencyContainer.getPriceListDataGateway();
        ARC_TDGW_PriceList thirdDepdendency = new ARC_TDGW_PriceListSObject();

        Test.stopTest();

        System.assertEquals(secondDependency, firstDependency);
        System.assertNotEquals(thirdDepdendency, firstDependency);
    }


    @IsTest
    private static void testGetPriceListItemDataGatewayReturnsSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_TDGW_PriceListItem firstDependency = dependencyContainer.getPriceListItemDataGateway();
        ARC_TDGW_PriceListItem secondDependency = dependencyContainer.getPriceListItemDataGateway();
        ARC_TDGW_PriceListItem thirdDepdendency = new ARC_TDGW_PriceListItemSObject();

        Test.stopTest();

        System.assertEquals(secondDependency, firstDependency);
        System.assertNotEquals(thirdDepdendency, firstDependency);
    }


    @IsTest
    private static void testGetProductDataGatewayReturnsSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_TDGW_Product firstDependency = dependencyContainer.getProductDataGateway();
        ARC_TDGW_Product secondDependency = dependencyContainer.getProductDataGateway();
        ARC_TDGW_Product thirdDepdendency = new ARC_TDGW_ProductSObject(
            dependencyContainer.getConfiguration().getProductConfiguration()
        );

        Test.stopTest();

        System.assertEquals(secondDependency, firstDependency);
        System.assertNotEquals(thirdDepdendency, firstDependency);
    }


    @IsTest
    private static void testGetProductStatusDataGatewayReturnsSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_TDGW_ProductStatus firstDependency = dependencyContainer.getProductStatusDataGateway();
        ARC_TDGW_ProductStatus secondDependency = dependencyContainer.getProductStatusDataGateway();
        ARC_TDGW_ProductStatus thirdDepdendency = new ARC_TDGW_ProductStatusSObject();

        Test.stopTest();

        System.assertEquals(secondDependency, firstDependency);
        System.assertNotEquals(thirdDepdendency, firstDependency);
    }


    @IsTest
    private static void testGetSeasonDataGatewayReturnsSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_TDGW_Season firstDependency = dependencyContainer.getSeasonDataGateway();
        ARC_TDGW_Season secondDependency = dependencyContainer.getSeasonDataGateway();
        ARC_TDGW_Season thirdDepdendency = new ARC_TDGW_SeasonSObject();

        Test.stopTest();

        System.assertEquals(secondDependency, firstDependency);
        System.assertNotEquals(thirdDepdendency, firstDependency);
    }


    @IsTest
    private static void testGetUserDataGatewayReturnsSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_TDGW_User firstDependency = dependencyContainer.getUserDataGateway();
        ARC_TDGW_User secondDependency = dependencyContainer.getUserDataGateway();
        ARC_TDGW_User thirdDepdendency = new ARC_TDGW_UserSObject();

        Test.stopTest();

        System.assertEquals(secondDependency, firstDependency);
        System.assertNotEquals(thirdDepdendency, firstDependency);
    }

    @IsTest
    private static void testGetProductStorefrontDataGatewayReturnsSameInstance ()
    {
        setUp();

        Test.startTest();

        ARC_TDGW_ProductStorefront firstDependency = dependencyContainer.getProductStorefrontDataGateway();
        ARC_TDGW_ProductStorefront secondDependency = dependencyContainer.getProductStorefrontDataGateway();
        ARC_TDGW_ProductStorefront thirdDepdendency = new ARC_TDGW_ProductStorefrontSObject();

        Test.stopTest();

        System.assertEquals(secondDependency, firstDependency);
        System.assertNotEquals(thirdDepdendency, firstDependency);
    }
}